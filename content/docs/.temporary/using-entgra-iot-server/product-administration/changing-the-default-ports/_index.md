# Changing the default ports

There are situations when the default port needs to be changed using a port offset  (e.g., when configuring Entgra IoT Server with WSO2 Identity Server). When the port offset is set, Entgra IoT Server starts on a different port, and it changes the default TCP port by increasing the value of the port. Therefore, for Entgra IoT Server to function properly the port in the TCP connection URL that corresponds to the various Entgra IoT Server related configuration files need to be updated.


## What is port offset

The port offset feature allows you to run multiple Entgra products, multiple instances of a Entgra product, or multiple Entgra product clusters on the same server or virtual machine (VM). The port offset defines the number by which all ports defined in the runtime, such as the HTTP/S ports, need to be offset. For example, if the HTTP port is defined as 9763 and the port Offset is 1, the effective HTTP port changes to 9764\. Therefore, for each additional Entgra product, instance, or cluster you add to a server, set the port offset to a unique value (the default is 0).


Entgra IoT Server compromises of the IoT core, analytics and broker profiles. By default, the three profiles have the following offsets:

<table>
  <colgroup>
    <col>
    <col>
    <col>
  </colgroup>
  <tbody>
    <tr>
      <th>Profile</th>
      <th>Offset</th>
      <th>Default port</th>
    </tr>
    <tr>
      <td>Core</td>
      <td>0</td>
      <td>9443/9763</td>
    </tr>
    <tr>
      <td>Analytics</td>
      <td>2</td>
      <td>9445/9765</td>
    </tr>
    <tr>
      <td>Broker</td>
      <td>3</td>
      <td>9446/9766</td>
    </tr>
  </tbody>
</table>

Make sure that the port offset of each profile differs from each other. No two profiles can have the same offset. Else, you will run into errors.



Follow the instructions below to enable port offset in Entgra IoT Server :

1.  Configure the `carbon.xml` files of the three profile to port offset.

    <table>
      <colgroup>
        <col>
        <col>
      </colgroup>
      <tbody>
        <tr>
          <th>Core</th>
          <td>
            <p>Open the <code>&lt;IOTS_HOME&gt;/conf/carbon.xml</code> file and port offset the IoT Server's core profile by configuring the <code>&lt;Offset&gt;</code> attribute.</p>
            <div class="confluence-information-macro confluence-information-macro-note"><span class="aui-icon aui-icon-small aui-iconfont-warning confluence-information-macro-icon"></span>
              <div class="confluence-information-macro-body">
                <p>Make sure not to have the same offset values as the broker or analytics profile.</p>
              </div>
            </div>
            <p>Example:</p>
            <div class="code panel pdl" style="border-width: 1px;">
              <div class="codeContent panelContent pdl">
                <pre class="syntaxhighlighter-pre" data-syntaxhighlighter-params="brush: java; gutter: false; theme: Confluence" data-theme="Confluence">&lt;Offset&gt;4&lt;/Offset&gt;</pre>
              </div>
            </div>
            <p>Now the HTTPS and HTTP ports of the core profile are 9447 and 9767.</p>
          </td>
        </tr>
        <tr>
          <th>Analytics</th>
          <td>
            <p>Open the <code>&lt;IOTS_HOME&gt;/wso2/analytics/conf/carbon.xml</code> file and port offset the IoT Server's core profile by configuring the <code>&lt;Offset&gt;</code> attribute.</p>
            <div class="confluence-information-macro confluence-information-macro-note"><span class="aui-icon aui-icon-small aui-iconfont-warning confluence-information-macro-icon"></span>
              <div class="confluence-information-macro-body">
                <p>Make sure not to have the same offset values as the broker or analytics profile.</p>
              </div>
            </div>
            <div class="code panel pdl" style="border-width: 1px;">
              <div class="codeContent panelContent pdl">
                <pre class="syntaxhighlighter-pre" data-syntaxhighlighter-params="brush: java; gutter: false; theme: Confluence" data-theme="Confluence">&lt;Offset&gt;5&lt;/Offset&gt;</pre>
              </div>
            </div>
            <p>Now the HTTPS and HTTP ports of the analytics profile are 9448 and 9768.</p>
          </td>
        </tr>
        <tr>
          <th>Broker</th>
          <td>
            <p>Open the <code>&lt;IOTS_HOME&gt;/wso2/broker/conf/carbon.xml</code> file and port offset the IoT Server's core profile by configuring the <code>&lt;Offset&gt;</code> attribute.</p>
            <div class="confluence-information-macro confluence-information-macro-note"><span class="aui-icon aui-icon-small aui-iconfont-warning confluence-information-macro-icon"></span>
              <div class="confluence-information-macro-body">
                <p>Make sure not to have the same offset values as the broker or analytics profile.</p>
              </div>
            </div>
            <div class="code panel pdl" style="border-width: 1px;">
              <div class="codeContent panelContent pdl">
                <pre class="syntaxhighlighter-pre" data-syntaxhighlighter-params="brush: java; gutter: false; theme: Confluence" data-theme="Confluence">&lt;Offset&gt;6&lt;/Offset&gt;</pre>
              </div>
            </div>
            <p>Now the HTTPS and HTTP ports of the broker profile are 9449 and 9769.</p>
          </td>
        </tr>
      </tbody>
    </table>

2.  Open the `<IOTS_HOME>/bin/``iot` `-server.sh` or `iot-server.bat` file configures the following properties:

    <table>
      <colgroup>
        <col>
        <col>
      </colgroup>
      <tbody>
        <tr>
          <th>Analytics ports</th>
          <td>
            <p>By default the analytics profile is offset by 2\. In step 1, you offset the analytics profile by 3 more again. Therefore, all the ports relevant to the analytics profile needs to be offset by 3 more.</p>
            <p>Example:</p>
            <div class="code panel pdl" style="border-width: 1px;">
              <div class="codeContent panelContent pdl">
                <pre class="syntaxhighlighter-pre" data-syntaxhighlighter-params="brush: java; gutter: false; theme: Confluence" data-theme="Confluence">-Diot.analytics.https.port="9448" \</pre>
              </div>
            </div>
          </td>
        </tr>
        <tr>
          <th>Broker ports</th>
          <td>
            <p>By default, the broker profile is offset by 3\. In step 1, you offset the broker profile by 3 more. Therefore, all the ports relevant to the broker profile needs to be offset by 3.</p>
            <p>Example:</p>
            <div class="code panel pdl" style="border-width: 1px;">
              <div class="codeContent panelContent pdl">
                <pre class="syntaxhighlighter-pre" data-syntaxhighlighter-params="brush: java; gutter: false; theme: Confluence" data-theme="Confluence">-Dmqtt.broker.port="1889" \</pre>
              </div>
            </div>
          </td>
        </tr>
        <tr>
          <th>IoT core ports</th>
          <td>
            <p>By default the core profile is offset by 0\. In step 1, you offset the broker profile by 4\. Therefore, all the ports relevant to the core profile needs to be offset by 4.<br>Example:</p>
            <div class="code panel pdl" style="border-width: 1px;">
              <div class="codeContent panelContent pdl">
                <pre class="syntaxhighlighter-pre" data-syntaxhighlighter-params="brush: java; gutter: false; theme: Confluence" data-theme="Confluence">-Diot.manager.https.port="9447" \
    -Diot.core.https.port="9447" \
    -Diot.keymanager.https.port="9447" \
    -Diot.gateway.https.port="8247" \
    -Diot.gateway.http.port="8284" \
    -Diot.gateway.carbon.https.port="9447" \
    -Diot.gateway.carbon.http.port="9767" \
    -Diot.apimpublisher.https.port="9447" \
    -Diot.apimstore.https.port="9447" \</pre>
              </div>
            </div>
          </td>
        </tr>
      </tbody>
    </table>

3.  Open the `<IOTS_HOME>/wso2/analytics/bin/wso2` `server.sh` or `wso2server.bat` file configures the following properties:

    <table>
      <colgroup>
        <col>
        <col>
      </colgroup>
      <tbody>
        <tr>
          <th>Broker ports</th>
          <td>
            <p>By default, the broker profile is offset by 3\. In step 1, you offset the broker profile by 3 more. Therefore, all the ports relevant to the broker profile needs to be offset by 3.</p>
            <p>Example:</p>
            <div class="code panel pdl" style="border-width: 1px;">
              <div class="codeContent panelContent pdl">
                <pre class="syntaxhighlighter-pre" data-syntaxhighlighter-params="brush: java; gutter: false; theme: Confluence" data-theme="Confluence">-Dmqtt.broker.port="1889" \</pre>
              </div>
            </div>
          </td>
        </tr>
        <tr>
          <th>IoT core ports</th>
          <td>
            <p>By default the core profile is offset by 0\. In step 1, you offset the core profile by 4\. Therefore, all the ports relevant to the core profile needs to be offset by 4.<br>Example:</p>
            <div class="code panel pdl" style="border-width: 1px;">
              <div class="codeContent panelContent pdl">
                <pre class="syntaxhighlighter-pre" data-syntaxhighlighter-params="brush: java; gutter: false; theme: Confluence" data-theme="Confluence">-Diot.keymanager.https.port="9447" \
    -Diot.gateway.https.port="8247" \</pre>
              </div>
            </div>
          </td>
        </tr>
      </tbody>
    </table>

4.  Open the `<IOTS_HOME>/wso2/broker/conf/broker.xml` file and configure the following properties:

    1.  The port that refers to the IoT core in the `hostURL` attribute that is in the `<authenticator class="org.wso2.carbon.andes.authentication.andes.OAuth2BasedMQTTAuthenticator">` class.  
        Example: 

        
        <authenticator class="org.wso2.carbon.andes.authentication.andes.OAuth2BasedMQTTAuthenticator">
           <property name="hostURL">https://localhost:9447/services/OAuth2TokenValidationService</property>
           <property name="username">admin</property>
           <property name="password">admin</property>
           <property name="maxConnectionsPerHost">10</property>
           <property name="maxTotalConnections">150</property>
        </authenticator>
        

    2.  The port that refers to the IoT core profile in the `tokenEndpoint` attribute that is in the `<org.wso2.carbon.andes.extensions.device.mgt.mqtt.authorization.DeviceAccessBasedMQTTAuthorize>` class.  
        Example:

        
        <authorizer class="org.wso2.carbon.andes.extensions.device.mgt.mqtt.authorization.DeviceAccessBasedMQTTAuthorizer">
           <property name="username">admin</property>
           <property name="password">admin</property>
           <property name="tokenEndpoint">https://localhost:8247</property>
           <!--offset time from expiry time to trigger refresh call - seconds -->
           <property name="tokenRefreshTimeOffset">100</property>
           <property name="deviceMgtServerUrl">https://localhost:8247</property>
        </authorizer>
        

5.  Configure following properties in the `<IOTS_HOME>/conf/identity/``sso-idp-config.xml` file.

    <table>
      <colgroup>
        <col style="width: 84.0px;">
        <col style="width: 1174.0px;">
      </colgroup>
      <tbody>
        <tr>
          <th>IoT core</th>
          <td>
          
            <p>By default the core profile is offset by 0\. In step 1, you offset the core profile by 4\. Therefore, all the ports relevant to core needs to be offset by 4\.<br>Example:</p>
 &lt;DefaultAssertionConsumerServiceURL&gt;https://localhost:9447/devicemgt/uuf/sso/acs&lt;/DefaultAssertionConsumerServiceURL&gt;
     &lt;Audience&gt;https://localhost:9447/oauth2/token&lt;/Audience&gt;
     &lt;Recipient&gt;https://localhost:9447/oauth2/token&lt;/Recipient&gt;
     &lt;AssertionConsumerServiceURL&gt;https://localhost:9447/store/acs&lt;/AssertionConsumerServiceURL&gt;
     &lt;DefaultAssertionConsumerServiceURL&gt;https://localhost:9447/store/acs&lt;/DefaultAssertionConsumerServiceURL&gt;
     &lt;AssertionConsumerServiceURL&gt;https://localhost:9447/social/acs&lt;/AssertionConsumerServiceURL&gt;
     &lt;DefaultAssertionConsumerServiceURL&gt;https://localhost:9447/social/acs&lt;/DefaultAssertionConsumerServiceURL&gt;
     &lt;AssertionConsumerServiceURL&gt;https://localhost:9447/publisher/acs&lt;/AssertionConsumerServiceURL&gt;
     &lt;DefaultAssertionConsumerServiceURL&gt;https://localhost:9447/publisher/acs&lt;/DefaultAssertionConsumerServiceURL&gt;
     &lt;AssertionConsumerServiceURL&gt;https://localhost:9447/api-store/jagg/jaggery_acs.jag&lt;/AssertionConsumerServiceURL&gt;
     &lt;DefaultAssertionConsumerServiceURL&gt;https://localhost:9447/api-store/jagg/jaggery_acs.jag&lt;/DefaultAssertionConsumerServiceURL&gt;
     &lt;AssertionConsumerServiceURL&gt;https://localhost:9447/portal/acs&lt;/AssertionConsumerServiceURL&gt;
     &lt;DefaultAssertionConsumerServiceURL&gt;https://localhost:9447/portal/acs&lt;/DefaultAssertionConsumerServiceURL&gt;
     &lt;Audience&gt;https://localhost:9447/oauth2/token&lt;/Audience&gt;
     &lt;Recipient&gt;https://localhost:9447/oauth2/token&lt;/Recipient&gt;
     &lt;Audience&gt;https://localhost:9447/oauth2/token&lt;/Audience&gt;
     &lt;Recipient&gt;https://localhost:9447/oauth2/token&lt;/Recipient&gt;
          </td>
        </tr>
        <tr>
          <th>Analytics</th>
          <td>
            <p>By default the analytics profile is offset by 2\. In step 1, you offset the analytics profile by 3 more again. Therefore, all the ports relevant to the analytics profile needs to be offset by 3 more.</p>
                        <p>Example:</p>
                        &lt;AssertionConsumerServiceURL&gt;https://localhost:9448/portal/acs&lt;/AssertionConsumerServiceURL&gt;
                            &lt;DefaultAssertionConsumerServiceURL&gt;https://localhost:9448/portal/acs&lt;/DefaultAssertionConsumerServiceURL&gt;
                            &lt;AssertionConsumerServiceURL&gt;https://localhost:9448/portal/acs&lt;/AssertionConsumerServiceURL&gt;
                &lt;DefaultAssertionConsumerServiceURL&gt;https://localhost:9448/portal/acs&lt;/DefaultAssertionConsumerServiceURL&gt;
                          
          </td>
        </tr>
      </tbody>
    </table>

6.  Open the `<IOTS_HOME>/repository/deployment/server/jaggeryapps/devicemgt/app/conf/app-conf.json` file and configure the following properties related to the IoT Server's core profile:  
    Example:  
    By default, the core profile is offset by 0\. In step 1, you offset the core profile by 4\. Therefore, all the ports relevant to core needs to be offset by 4.

    `"portalURL": "https://${server.ip}:9447",`

7.  Open the ``<IOTS_HOME>/repository/deployment/server/jaggeryapps/api-store/site/conf/site.json`` file and configure the following properties and configure the following properties related to the IoT Server's core profile:  
    Example:  
    By default, the core profile is offset by 0\. In step 1, you offset the core profile by 4\. Therefore, all the ports relevant to core needs to be offset by 4. 

    js
    "identityProviderURL": "https://localhost:9447/samlsso",
    "identityProviderURI": "http://localhost:8084/openid-connect-server-webapp/",
    "authorizationEndpointURI": "http://localhost:8084/openid-connect-server-webapp/authorize",
    "tokenEndpointURI": "http://localhost:8084/openid-connect-server-webapp/token",
    "userInfoURI": "http://localhost:8084/openid-connect-server-webapp/userinfo",
    "jwksURI": "http://localhost:8084/openid-connect-server-webapp/jwk",
    

8.  Open the `<IOTS_HOME>/conf/etc/webapp-publisher-config.xml` file, and set `true` as the value for `<EnabledUpdateApi>`.

    
    <!-- If it is true, the APIs of this instance will be updated when the webapps are redeployed -->
    <EnabledUpdateApi>true</EnabledUpdateApi>
    


    If you have not started Entgra IoT Server previously, you don't need this configuration. When the server starts for the first time it will update the APIs and web apps with the new ports.


    Make sure to configure this property back to `false` if you need to restart the server again after the configuring the ports.   

    By enabling the update API property, the APIs and the respective web apps get updated when the server restarts. This takes some time. Therefore, if you need to restart the server many times after this configuration or when in a production environment, you need to revert back to the default setting.


Congratulations! You have successfully port offset Entgra IoT Server's core, analytics and broker profiles. You can now start the three profiles and manage your devices.
