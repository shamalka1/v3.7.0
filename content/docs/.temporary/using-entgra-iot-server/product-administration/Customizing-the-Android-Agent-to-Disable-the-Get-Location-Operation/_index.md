# Customizing the Android Agent to Disable the Get Location Operation


The Android agent will retrieve the device location each time the get device information operation runs on the client side, even though you don't specifically run the get device location operation. Follow the steps given below to disable the get location operation when running the get device information operation.

1.  [Download the Android agent source code](https://github.com/wso2/cdmf-agent-android/releases/tag/v2.0.0). This will be referred to as `<ANDROID_AGENT_SOURCE_CODE>`.

2.  Open the `AndroidManifest.xml` file in the `<ANDROID_AGENT_SOURCE_CODE>/client/client/src/main` directory via Android Studio.
3.  Comment out the `ACCESS_GPS`, `ACCESS_FINE_LOCATION` and `ACCESS_COARSE_LOCATION` android permissions.   
    Example:

    
    <!-- <uses-permission android:name="android.permission.ACCESS_GPS" />
    <uses-permission android:name="android.permission.ACCESS_FINE_LOCATION" />
    <uses-permission android:name="android.permisssion.ACCESS_COARSE_LOCATION" />-->
    

4.  The Android agent can't be built via the usual android developer SDK, as it requires access to [developer restricted APIs](about:blank#). Therefore, you need to replace the existing `android.jar` file that is in the `<SDK_LOCATION>/platforms/android-<COMPILE_SDK_VERSION>` directory with the explicitly built `android.jar` file that has access to the restricted APIs.   
    You can get the new `jar` file using one of the following methods:

    *   [Download the Android Open Source Project (AOSP)](https://source.android.com/compatibility/cts/downloads.html) and build the source code to get the `jar` file for the required SDK level.

    *   Use a pre-built jar file from a third party developer. You can find it here: [https://github.com/anggrayudi/android-hidden-api](https://github.com/anggrayudi/android-hidden-api).
5.  Build the project to create a new APK file that has all the changes you made.

6.  Rename the created `.apk` file to `android-agent.apk`.
7.  Copy the renamed file and replace it instead of the existing `android-agent.apk` file that is in the `<IOTS_HOME>` `/repository/deployment/server/jaggeryapps/devicemgt/app/units/cdmf.unit.device.type.android.type-view/public/assets` directory.


