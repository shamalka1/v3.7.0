# Checking Notifications


There will be situations where you want certain operations to be carried out on a device but that device will not be able to support the operation. The failure to carry out such operations will be notified to the administrator and the device owner. There will also be instances where the user will need to be notified of additional information such as the new lock reset pin.

The example given below will help you understand how the administrator and the device owner also know as privileged users will be notified of an operation failure:

**Example**: You wish to carry out the device lock operation on your Windows device that is registered with Entgra IoTS but the respective device does not have a device lock enabled (it does not have a pin or a security pattern enabled). Therefore when you carry out the device lock operation, the operation will fail.

*   When the operation fails you will be notified via a notification.   
    ![image](352821264.png)
*   To check the notification, click the notification icon.

    

    

    The unread notifications are shown under **Unread** and the notifications that have already been read will be shown under **All Notifications**.

    

    

    ![image](352821269.png)

*   When the device lock operation fails the admin or device owner is advised to reset the lock on the device. To carry out the lock-rest operation click the view icon of the respective notification. 

    

    

    If you have more than one device enrolled in Entgra IoTS you can navigate to the respective device on, which the operation failed, by clicking the view icon.

    

    

    ![image](352821284.png)

*   Once navigated to the respective device page, as mentioned in the notification carry out the lock-reset operation.
*   EMM will notify the administrator and the device owner of the new device pin. Use the given pin to unlock the device.

    

    

    Click the view icon, if you wish to know on, which device the lock-reset operation was carried out.

    

    

    ![image](352821274.png)
    
