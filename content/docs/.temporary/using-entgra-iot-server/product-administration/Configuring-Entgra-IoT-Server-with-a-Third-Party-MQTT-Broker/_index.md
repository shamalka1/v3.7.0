
# Configuring Entgra IoT Server with a Third Party MQTT Broker

Entgra IoT Server is prepackaged with a default broker profile to handle message brokering. This section presents how you can use a third party MQTT broker instead of the default Entgra IoT Server broker profile. This is useful when running the Entgra IoT Server device management profile in a production environment that already uses a third party MQTT broker.

## Implementation

The broker profile enables devices to communicate with the Entgra IoT Server device management profile in a secure fashion. MQTT implementation by default uses basic auth to authenticate a client and it requires hard coding Device Owner credentials to the device. This is not a safe device authentication mechanism. Therefore, Entgra IoT Server recommends OAuth for device authentication and authorization. 

To implement authorization, Entgra IoT Server maintains a topic structure, where each device is associated with a unique topic pattern as shown below. You can trace a device using this approach.

`<TENANT_DOMAIN>/<DEVICE_TYPE_NAME>/<DEVICE_ID>/...`

When a client tries to publish/subscribe to a topic, the device management core profile verifies whether the client is authorized to access the device as shown in the Device Communication Sequence diagram. 

*   **Connect to a broker**: When a client (e.g., device, analytics profile) attempts to connect to the Device Management profile via the Broker profile, the Key Manager authenticates the client via the **TokenValidationService** service.
*   **Subscribe/Publish to a topic**:  Once authenticated, the client attempts to subscribe/publish to a topic by communicating to the Device Management profile. The Device Management profile authorizes the device user via the **DeviceAccessAuthorizationService** service. Once authorized, the device is granted publishing/subscribing to the topic. 

![image](352821819.png)

## Configurations

Follow the steps below to configure Entgra IoT Server with a third-party MQTT broker.

1.  Copy the following code snippet to the **TokenValidationService** SOAP service of the respective MQTT broker.

    
    <dependency>
       <groupId>org.wso2.carbon.identity</groupId>
       <artifactId>org.wso2.carbon.identity.oauth.stub</artifactId>
       <version>${carbon.identity.oauth.stub.version}</version>
    </dependency>


    

    

    This enables a client stub to access the TokenValidationService. To check out the existing code implementation, see [OAuth2BasedMQTTAuthenticator.java](https://github.com/wso2/carbon-business-messaging/blob/v3.2.15/components/andes/org.wso2.carbon.andes.authentication/src/main/java/org/wso2/carbon/andes/authentication/andes/OAuth2BasedMQTTAuthenticator.java).

    

    

2.  Call the **DeviceAccessAuthorizationService** API, to authorize the client.

    Example:

    `curl -X POST https://localhost:8243/api/device-mgt/v1.0/admin/authorization -H 'authorization: Bearer 7e5cad0f-cf78-3981-b50e-db9d674fb741' -H 'content-type: application/json' -d '{"tenantDomain":"carbon.super","username": "ayyoob","deviceIdentifiers":[{"id":123, "type":"firealarm"}]}'`

    

    

    To check out the existing code implementation, see [DeviceAccessBasedMQTTAuthorizer.java](https://github.com/wso2/carbon-device-mgt-plugins/blob/master/components/extensions/mb-extensions/org.wso2.carbon.andes.extensions.device.mgt.mqtt.authorization/src/main/java/org/wso2/carbon/andes/extensions/device/mgt/mqtt/authorization/DeviceAccessBasedMQTTAuthorizer.java).

    

    
