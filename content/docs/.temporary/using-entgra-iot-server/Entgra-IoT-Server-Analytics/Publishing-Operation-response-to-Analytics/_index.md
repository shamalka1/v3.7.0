# Publishing Operation response to Analytics

Using Entgra IoT analytics you are able to publish all the responses or selectively publish operation responses to the `org.wso2.iot.OperationResponseStream`. By default, the operation response publishing configuration is disabled. Let's take a look at how you can enable it and publish the operation responses.

### Publishing all operation responses

Follow the steps given below to publish the all the operation responses:

1.  Open the `cdm-config.xml` file that is in the `<IOTS_HOME>/conf` directory.
2.  Configure the `Enabled` parameter that is under `PublishOperationResponse` as `true`. 

Example: Publishing all operation responses to the `org.wso2.iot.OperationResponseStream`.


<OperationAnalyticsConfiguration>
    <PublishLocationResponse>false</PublishLocationResponse>
    <PublishDeviceInfoResponse>false</PublishDeviceInfoResponse>
    <PublishOperationResponse>
        <Enabled>true</Enabled>
        <Operations>
            <Operation>*</Operation>
        </Operations>
    </PublishOperationResponse>
</OperationAnalyticsConfiguration>

### Publishing selected operation responses

Follow the steps given below to publish a selected set of operation responses:

1.  Open the `cdm-config.xml` file that is in the `<IOTS_HOME>/conf` directory.
2.  Configure the `Enabled`  parameter that is under `PublishOperationResponse` as `true`.
3.  Specify the set of operation responses, such as the `BATTERY_LEVEL` and `CHECK_LOCK_STATUS`, under `Operations`.

Example: Publishing the `BATTERY_LEVEL` and `CHECK_LOCK_STATUS` operation responses to `org.wso2.iot.OperationResponseStream`.

<OperationAnalyticsConfiguration>
    <PublishLocationResponse>false</PublishLocationResponse>
    <PublishDeviceInfoResponse>false</PublishDeviceInfoResponse>
    <PublishOperationResponse>
        <Enabled>true</Enabled>
        <Operations>
            <Operation>BATTERY_LEVEL</Operation>
            <Operation>CHECK_LOCK_STATUS</Operation>
        </Operations>
    </PublishOperationResponse>
</OperationAnalyticsConfiguration>
