# Extending Entgra IoT Server


The following sub sections explain how to use the features of Entgra IoT Server and extend it to meet your business requirement:

*   [Writing the Device Feature](https://entgra-documentation.gitlab.io/v3.7.0/docs/using-entgra-iot-server/device-manufacturer-guide/extending-entgra-iot-server/#writing-the-device-feature)
*   [Writing Device Types](https://entgra-documentation.gitlab.io/v3.7.0/docs/using-entgra-iot-server/device-manufacturer-guide/extending-entgra-iot-server/#writing-the-device-types)
*   [Writing Device APIs](https://entgra-documentation.gitlab.io/v3.7.0/docs/using-entgra-iot-server/device-manufacturer-guide/extending-entgra-iot-server/#writing-device-apis)
*   [Writing Transport Extensions](https://entgra-documentation.gitlab.io/v3.7.0/docs/using-entgra-iot-server/device-manufacturer-guide/extending-entgra-iot-server/#writing-transport-extensions)
*   [Writing UI Extensions](https://entgra-documentation.gitlab.io/v3.7.0/docs/using-entgra-iot-server/device-manufacturer-guide/extending-entgra-iot-server/#writing-ui-extensions)
*   [Writing Analytics](https://entgra-documentation.gitlab.io/v3.7.0/docs/using-entgra-iot-server/device-manufacturer-guide/extending-entgra-iot-server/#writing-analytics)
*   [Writing Device Agents](https://entgra-documentation.gitlab.io/v3.7.0/docs/using-entgra-iot-server/device-manufacturer-guide/extending-entgra-iot-server/#writing-device-agents)
*   [Creating a New Device Type via the Maven Archetype](https://entgra-documentation.gitlab.io/v3.7.0/docs/using-entgra-iot-server/device-manufacturer-guide/extending-entgra-iot-server/#creating-a-new-device-type-via-the-maven-archetype)

# Writing the Device Feature
A collection of [Carbon](http://docs.wso2.com/carbon) components are required to create a new device type in Entgra IoT Server. It is developed simply by plugging various Carbon components that provide different features. The following folder structure is recommended to create the feature, but you are able to customize it to suit your requirement.

Example: The folder structure of the Raspberry Pi plugin that is in the `<CARBON_DEVICE_MGT_PLUGINS>/features` directory.

![image](352822014.png)



Before you begin



1.  [Download Entgra IoT Server](https://storage.googleapis.com/iot-release-public/3.4.0/entgra-iots-3.4.0.zip).

2.  Clone and build the WSO2 Carbon Device Management Plugins repository:

*   *   Clone the WSO2 Carbon Device Management Plugins repository. It will be referred to as `<CARBON_DEVICE_MGT_PLUGINS>` throughout this document.

        `git clone https://github.com/wso2/carbon-device-mgt-plugins.git`

    *   Build the repo you just cloned.

        
        cd <CARBON_DEVICE_MGT_PLUGINS>
        mvn clean install
        

Let's take a look at how each directory contributes to your device type and why it's used:

## Agent directory

When enrolling a device with Entgra IoT Server you require agents to communicate with the device and the server. You can configure the agent and host it on a separate location or have it within Entgra IoT Server. If you are configuring the agent via Entgra IoT Server all the agent related files can be included in this directory. 

This directory is not mandatory. Create this directory only if your device type requires an agent to communicate with the device and the server.

More Inofrmation

For more information, see [Writing Device Agents](https://entgra-documentation.gitlab.io/v3.7.0/docs/using-entgra-iot-server/device-manufacturer-guide/extending-entgra-iot-server/#writing-device-agents).

Each device type has its own enrollment mechanism. Take a look at the different ways of enrolling devices:

*   Enrolling via a Software Development Kit (SDK).  
    Example: This mechanism is used when enrolling an Android device with Entgra IoTS.
*   Enrolling a device my installing a sketch or an agent on a device.  
    Example: Enrolling a Raspberry Pi or Arduino device

1.  Define the device configurations in the deviceConfig.properties as shown below:

    The properties in the file will be replaced with the related information when the agent is being downloaded so that the information can be used to communicate with the IoT Server and the agent.

    Example: Configurations for Raspberry Pi

    [Device-Configurations]
    server-name=${SERVER_NAME}
    owner=${DEVICE_OWNER}
    deviceId=${DEVICE_ID}
    device-name=${DEVICE_NAME}
    controller-context=/raspberrypi/controller
    mqtt-sub-topic=${SERVER_NAME}/{owner}/raspberrypi/{deviceId}
    mqtt-pub-topic=${SERVER_NAME}/{owner}/raspberrypi/{deviceId}/publisher
    https-ep=${HTTPS_EP}
    http-ep=${HTTP_EP}
    apim-ep=${APIM_EP}
    mqtt-ep=${MQTT_EP}
    xmpp-ep=${XMPP_EP}
    auth-method=token
    auth-token=${DEVICE_TOKEN}
    refresh-token=${DEVICE_REFRESH_TOKEN}
    push-interval=15
    

2.  Create the sketch.properties. It contains the information for the IoT Server to find the agent template that was explained in step 1 and where to get the zip file when a user clicks download agent.

    templates = deviceConfig.properties
    zipfilename = RaspberryPiAgent.zip
   

3.  You might require scripts to start the downloaded agent. Add all the configurations and files required for the agent in this directory.

Please note that this is just an example. You can configure your agent anyway your prefer. Take a look at the Arduino agent directory to see another way of how the agent is configured.

## Datasources directory

A datasource provides the information that is required for a server to connect to a database or to an external data store.

For more information, see [Configuring the master-datasources.xml](https://docs.wso2.com/display/ADMIN44x/Configuring+master-datasources.xml).

<?xml version="1.0" encoding="UTF-8"?>
<datasources-configuration xmlns:svns="http://org.wso2.securevault/configuration">
   <providers>
      <provider>org.wso2.carbon.ndatasource.rdbms.RDBMSDataSourceReader</provider>
   </providers>
   <datasources>
      <datasource>
         <name>RaspberryPi_DB</name>
         <description>The datasource used for the RaspberryPi database</description>
         <jndiConfig>
            <name>jdbc/RaspberryPiDM_DB</name>
         </jndiConfig>
         <definition type="RDBMS">
            <configuration>
               <url>jdbc:h2:repository/database/RaspberryPiDM_DB;DB_CLOSE_ON_EXIT=FALSE</url>
               <username>wso2carbon</username>
               <password>wso2carbon</password>
               <driverClassName>org.h2.Driver</driverClassName>
               <maxActive>50</maxActive>
               <maxWait>60000</maxWait>
               <testOnBorrow>true</testOnBorrow>
               <validationQuery>SELECT 1</validationQuery>
               <validationInterval>30000</validationInterval>
            </configuration>
         </definition>
      </datasource>
   </datasources>
</datasources-configuration>








## DB scripts directory

Entgra IoTS runs on top of WSO2 CDMF. Each device type that you configure to enroll with Entgra IoTS will have details specific to them. Therefore you need to create the required database scripts in the `dbscripts` folder for these details to be passed into CDMF to create the respective databases in CDMF.

When a plugin is created it will get access to the following common APIs that is made available via WSO2 CDMF. These APIs too will refer to the database to know which fields need to be updated when one of the respective APIs are called. The APIs are:

*   Adding a device.
*   Updating the device details.
*   Getting the device.
*   Getting all the devices in Entgra IoTS.
*   Removing the device.


-- -----------------------------------------------------
-- Table `RASPBERRYPI_DEVICE`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `RASPBERRYPI_DEVICE` (
  `RASPBERRYPI_DEVICE_ID` VARCHAR(45) NOT NULL ,
  `DEVICE_NAME` VARCHAR(100) NULL DEFAULT NULL,
  PRIMARY KEY (`RASPBERRYPI_DEVICE_ID`) );


## Device types

All device types generally have common elements, such as specific attributes, feature management methods, and transport sender mechanisms. Entgra IoTS identified these common areas and created a template that includes all these details. Using this template you can create your own device plugin. 

For more information, see [Writing Device Type via the Template](https://entgra-documentation.gitlab.io/v3.7.0/docs/using-entgra-iot-server/device-manufacturer-guide/extending-entgra-iot-server/#writing-device-type-via-the-template). 
If your device requires special functions and need to be customized to meet a given requirement, see [Writing Device Plugins via Java Code](https://entgra-documentation.gitlab.io/v3.7.0/docs/using-entgra-iot-server/device-manufacturer-guide/extending-entgra-iot-server/#writing-device-plugins-via-java-code) for more information.


<DeviceTypeConfiguration name="raspberrypi">
    <Features>
        <Feature code="bulb">
            <Name>Control Bulb</Name>
            <Description>Control Bulb on Raspberrypi</Description>
            <Operation context="/raspberrypi/device/{deviceId}/bulb" method="POST">
                <QueryParameters>
                    <Parameter>state</Parameter>
                </QueryParameters>
            </Operation>
        </Feature>
    </Features>
    <ProvisioningConfig>
        <SharedWithAllTenants>false</SharedWithAllTenants>
    </ProvisioningConfig>
    <PushNotificationProvider type="MQTT">
        <FileBasedProperties>true</FileBasedProperties>
        <!--if file based properties is set to false then the configuration will be picked from platform configuration-->
        <ConfigProperties>
            <Property Name="mqtt.adapter.name">raspberrypi.mqtt.adapter</Property>
            <Property Name="url">tcp://localhost:1886</Property>
            <Property Name="username">admin</Property>
            <Property Name="dcrUrl">https://localhost:9443/dynamic-client-web/register</Property>
            <Property Name="qos">0</Property>
            <Property Name="scopes"/>
            <Property Name="clearSession">true</Property>
        </ConfigProperties>
    </PushNotificationProvider>
    <License>
        <Language>en_US</Language>
        <Version>1.0.0</Version>
        <Text>This is license text</Text>
    </License>
</DeviceTypeConfiguration>

   
p2.inf file

The `p2.inf` file contains the details on copying the files from the P2 repository to the Entgra IoTS pack. 


instructions.configure = \
org.eclipse.equinox.p2.touchpoint.natives.mkdir(path:${installFolder}/../../deployment/server/webapps/);\
org.eclipse.equinox.p2.touchpoint.natives.copy(source:${installFolder}/../features/org.wso2.carbon.device.mgt.iot.raspberrypi_${feature.version}/webapps/,target:${installFolder}/../../deployment/server/webapps/,overwrite:true);\
org.eclipse.equinox.p2.touchpoint.natives.mkdir(path:${installFolder}/../../resources/sketches/);\
org.eclipse.equinox.p2.touchpoint.natives.mkdir(path:${installFolder}/../../resources/sketches/raspberrypi/);\
org.eclipse.equinox.p2.touchpoint.natives.copy(source:${installFolder}/../features/org.wso2.carbon.device.mgt.iot.raspberrypi_${feature.version}/agent/,target:${installFolder}/../../resources/sketches/raspberrypi/,overwrite:true);\
org.eclipse.equinox.p2.touchpoint.natives.copy(source:${installFolder}/../features/org.wso2.carbon.device.mgt.iot.raspberrypi_${feature.version}/dbscripts/,target:${installFolder}/../../../dbscripts/cdm/plugins/raspberrypi,overwrite:true);\
org.eclipse.equinox.p2.touchpoint.natives.mkdir(path:${installFolder}/../../deployment/server/jaggeryapps/);\
org.eclipse.equinox.p2.touchpoint.natives.copy(source:${installFolder}/../features/org.wso2.carbon.device.mgt.iot.raspberrypi_${feature.version}/jaggeryapps/,target:${installFolder}/../../deployment/server/jaggeryapps/,overwrite:true);\
org.eclipse.equinox.p2.touchpoint.natives.copy(source:${installFolder}/../features/org.wso2.carbon.device.mgt.iot.raspberrypi_${feature.version}/datasources/,target:${installFolder}/../../conf/datasources/,overwrite:true);\
org.eclipse.equinox.p2.touchpoint.natives.mkdir(path:${installFolder}/../../database/);\
org.eclipse.equinox.p2.touchpoint.natives.copy(source:${installFolder}/../features/org.wso2.carbon.device.mgt.iot.raspberrypi_${feature.version}/database/,target:${installFolder}/../../database/,overwrite:true);\
org.eclipse.equinox.p2.touchpoint.natives.mkdir(path:${installFolder}/../../deployment/server/carbonapps/);\
org.eclipse.equinox.p2.touchpoint.natives.copy(source:${installFolder}/../features/org.wso2.carbon.device.mgt.iot.raspberrypi_${feature.version}/carbonapps/,target:${installFolder}/../../deployment/server/carbonapps/,overwrite:true);\
org.eclipse.equinox.p2.touchpoint.natives.mkdir(path:${installFolder}/../../deployment/server/devicetypes/);\
org.eclipse.equinox.p2.touchpoint.natives.copy(source:${installFolder}/../features/org.wso2.carbon.device.mgt.iot.raspberrypi_${feature.version}/devicetypes/,target:${installFolder}/../../deployment/server/devicetypes/,overwrite:true);\

instructions.unconfigure = \
org.eclipse.equinox.p2.touchpoint.natives.remove(path:${installFolder}/../../deployment/server/webapps/raspberrypi.war);\
org.eclipse.equinox.p2.touchpoint.natives.remove(path:${installFolder}/../../deployment/server/webapps/raspberrypi);\
org.eclipse.equinox.p2.touchpoint.natives.remove(path:${installFolder}/../../../dbscripts/cdm/plugins/raspberrypi);\
org.eclipse.equinox.p2.touchpoint.natives.remove(path:${installFolder}/../../resources/sketches/raspberrypi);\
org.eclipse.equinox.p2.touchpoint.natives.remove(path:${installFolder}/../../conf/datasources/raspberrypi-datasources.xml);\
org.eclipse.equinox.p2.touchpoint.natives.remove(path:${installFolder}/../../database/RaspberryPiDM_DB.h2.db);\
org.eclipse.equinox.p2.touchpoint.natives.remove(path:${installFolder}/../../deployment/server/jaggeryapps/devicemgt/app/units/cdmf.unit.device.type.raspberrypi.device-view);\
org.eclipse.equinox.p2.touchpoint.natives.remove(path:${installFolder}/../../deployment/server/jaggeryapps/devicemgt/app/units/cdmf.unit.device.type.raspberrypi.type-view);\
org.eclipse.equinox.p2.touchpoint.natives.remove(path:${installFolder}/../../deployment/server/jaggeryapps/devicemgt/app/units/cdmf.unit.device.type.raspberrypi.analytics-view);\
org.eclipse.equinox.p2.touchpoint.natives.remove(path:${installFolder}/../../deployment/server/jaggeryapps/devicemgt/app/units/cdmf.unit.device.type.raspberrypi.realtime.analytics-view);\
org.eclipse.equinox.p2.touchpoint.natives.remove(path:${installFolder}/../../deployment/server/jaggeryapps/devicemgt/app/units/cdmf.unit.device.type.raspberrypi.platform.configuration);\
org.eclipse.equinox.p2.touchpoint.natives.remove(path:${installFolder}/../../deployment/server/carbonapps/raspberrypi.car);\
org.eclipse.equinox.p2.touchpoint.natives.remove(path:${installFolder}/../../deployment/server/devicetypes/raspberrypi.xml);\


### pom.xml file

Create the `pom.xml` file to package the new feature and create the device type as a feature in the P2 repository. The file contains information on the device type and configuration details used by Maven to build the feature. 

Example: The fully configured `pom.xm`l file for the Raspberry Pi sample [here](https://github.com/wso2-incubator/product-iot-server/blob/master/modules/samples/currentsensor/feature/feature/pom.xml).

To get a better understanding on what was explained up to this point, see [Developing a Carbon Feature](https://docs.wso2.com/display/Carbon443/Developing+a+Carbon+Feature). Follow the sample given in this documentation and take a look at [Step 3: Packaging the New Feature](https://docs.wso2.com/display/Carbon443/Step+3%3A+Packaging+the+New+Feature) to know more on how to configure the pom.xml file.

### plugins-deployer.xml file

The `plugins-deployer.xml` contains the information required to create the P2 repository in the device types folder. Configure the `<IoT_HOME>/plugins/plugins-deployer.xml` file.

1.  Add the device type feature under the `<featureArtifacts>` tag.

    
    <featureArtifactDef>
       {groupID}-plugin:{groupID}.{artifactID}.feature:{version}
    </featureArtifactDef>
    

    Example:

    
    <featureArtifactDef>
       org.wso2.carbon.devicemgt-plugins:org.wso2.carbon.device.mgt.iot.raspberrypi.feature:${carbon.device.mgt.plugin.version}
    </featureArtifactDef>
    

2.  Add the device type feature group under the `<features>` tag.

    
    <features>
     <feature>
        <id>{groupID}.{artifactID}.feature.group</id>
        <version>{version}</version>
     </feature>
    </features>
    

    Example:

    
    <features>
     <feature>
        <id>org.wso2.carbon.device.mgt.iot.raspberrypi.feature.group</id>
        <version>${carbon.device.mgt.plugin.version}</version>
     </feature>
    </features>
    

# Writing the Device Types

Before you enroll a device with Entgra IoT Server you need to write a device type for it. You can write the device type using any of the following options:

*   Want to create a device type in one go? You can now create your own device type that includes the APIs, transports, UI and more, using the device management console or APIs. For more information, see **[Creating a New Device Type](hhttps://entgra-documentation.gitlab.io/v3.7.0/docs/tutorials/creating-a-new-device-type/)**.

*   Does your device have common functionalities similar to our sample device types? If yes, see **[Writing Device Type via the Template](https://entgra-documentation.gitlab.io/v3.7.0/docs/using-entgra-iot-server/device-manufacturer-guide/extending-entgra-iot-server/#writing-device-type-via-the-template)**.
*   Does your device require special functions and need to be customized to meet a given requirement? See **[Writing Device Plugins via Java Code](https://entgra-documentation.gitlab.io/v3.7.0/docs/using-entgra-iot-server/device-manufacturer-guide/extending-entgra-iot-server/#writing-device-plugins-via-java-code)**.

## Creating a New Device Type via the Device Management Console

**In this tutorial**, you are going to create a new device type, enroll a new device using the device management console and start using the device.

Before you begin

You need to start the Entgra IoT Server core and analytics profiles.


cd <IOTS_HOME>/bin  
------Linux/Mac OS/Solaris ----------
./iot-server.sh
./analytics.sh

-----Windows-----------
iot-server.bat
analytics.bat


Let's get started!

### Creating a new device type

Follow the steps given below to create a new device type via the console:

1.  Sign in to the Entgra Device Management Console.
2.  Click **Add** under **DEVICE TYPES**.

    Entgra IoT Server has the Android, Windows, Raspberry Pi, Arduino, Android Sense and iOS sample device types by default. If you have not run the `<IOTS_HOME>/samples/device-plugins-deployer.xml` file, you will only have the Android, Android Sense, and Windows device type by default.

3.  Enter the details to add a new device type and click **Add Device Type**.

    <table style="width: 80.0772%;">
      <colgroup>
        <col>
        <col>
      </colgroup>
      <tbody>
        <tr>
          <th>Name</th>
          <td>The name of the device type you are creating.</td>
        </tr>
        <tr>
          <th>Description</th>
          <td>A small introduction on your device type. It should not be very long.</td>
        </tr>
        <tr>
          <th>Push Notification Transport</th>
          <td>The communication method the server needs to use to send messages to the device.<br>
            <ul>
              <li>If you select NONE, there will be no transport method implemented for the server to communicate with the device.</li>
              <li>If you select MQTT, the MQTT transport is extended for the server to communicate with your device.</li>
            </ul>
          </td>
        </tr>
        <tr>
          <th>Features</th>
          <td><span>Feathure refers to the operations or functions your device can perform. For example, if your device is a </span>fire<span> alarm, it needs to ring when the temperature rises too high.</span> You can add more than one feature by clicking the <strong>+</strong> button.</td>
        </tr>
        <tr>
          <th>Feature name</th>
          <td>Provide a name for the device operation.This name is used to identify the device operation.<br>As per the above example, the feature name would be fire-alert.</td>
        </tr>
        <tr>
          <th>Feature code</th>
          <td>Provide the code to trigger the device operation. For example, ring is used as the code to trigger the fire-alert operation.</td>
        </tr>
        <tr>
          <th>Feature description</th>
          <td>Provide a description of the device operation.</td>
        </tr>
        <tr>
          <th>
            
              <p><span class="confluence-anchor-link" id="CreatingaNewDeviceTypeviatheDeviceManagementConsole-attributes"></span>Attributes</p>
            
          </th>
          <td>
            <p>The attributes unique to the device. <span>You can add more than one attribute by clicking the </span><strong>+</strong><span> button.</span> For example, a fire alarm will be placed in a specific building and a specific floor. Then the building_ID and the floor_ID will become it's attribute.</p>
            <p>Let's look at another example. If your device type is a car, the make and the year of manufacture will be the device's attributes.</p>
          </td>
        </tr>
      </tbody>
    </table>

    ![images](352822063.png)

4.  Create the event stream to gather the sensor readings or data from the device and click **Add**.

    <table>
      <colgroup>
        <col>
        <col>
      </colgroup>
      <tbody>
        <tr>
          <th>Transport</th>
          <td>The transport that needs be used by the server to communicate with the device.</td>
        </tr>
        <tr>
          <th>Event attributes</th>
          <td>Define the attributed of the event stream that needs to be created to gather the data from the device.<br><span>You can add more than one attribute by clicking the </span><strong>+</strong><span> button.</span></td>
        </tr>
        <tr>
          <th>Event name</th>
          <td>The name of the event stream that needs to be created.</td>
        </tr>
        <tr>
          <th>Event data type</th>
          <td>The type of the data that is gathered by the event stream.</td>
        </tr>
      </tbody>
    </table>

    ![image](352822052.png)

5.  Your device type is successfully created. Click **Go To Enrollment Page** to start enrolling a new device for the device type you created.

### Enrolling a new device

Follow the steps given below to enroll a new device for the device type you just created:

1.  On the Enroll Device page, click on your device type.
2.  Click **+ Create Device**.
3.  Enter the details to create the device and click **Create Device**. The details that need to be entered varies based on the [attribute values you defined when creating the device type](https://entgra-documentation.gitlab.io/v3.7.0/docs/using-entgra-iot-server/device-manufacturer-guide/extending-entgra-iot-server/#creating-a-new-device-type-via-the-device-management-console). 
    For example, when creating the fire alarm, the Building_ID and Floor_ID were added as attributes. Therefore, you need to enter the values when creating the new device.  
    ![image](352822058.png)  
    A JSON file containing the client_ID and client_secret key downloads. You can carry out various operations on the device via APIs. For more information, see [Try out more operations](https://entgra-documentation.gitlab.io/v3.7.0/docs/tutorials/creating-a-new-device-type/creating-a-device-type-via-apis/#try-out-more-operations).
    Or go to the devices page and carry out operations on the device. The operations that are there are the features you defined when creating the new device type.

## Writing Device Type via the Template

Before you begin



*   Clone the Entgra Carbon Device Management Plugins repository. It will be referred to as `<CARBON_DEVICE_MGT_PLUGINS>` throughout this document.

    `git clone https://github.com/wso2/carbon-device-mgt-plugins.git`

*   Build the repo you just cloned.

    
    cd <CARBON_DEVICE_MGT_PLUGINS>
    mvn clean install
    

All device types generally have common elements, such as specific attributes, feature management methods, and transport sender mechanisms. Entgra IoT Server identified these common areas and created a template that includes all these details. Using this template you can create your own device type. Let's take a quick look at how the devicetype is created from the template:

1.  Configure the [`device-type-template.xml`](https://github.com/wso2/carbon-device-mgt/blob/v3.0.69/components/device-mgt/org.wso2.carbon.device.mgt.extensions/src/test/resources/sample.xml) file to meet your device type's requirement.
2.  If your device type requires specific attributes, create a database and the required table to add these attribute details.
3.  Add the configured device type template to the `<CARBON_DEVICE_MGT_PLUGINS>/features/device-types-feature/<DEVICE>-feature-plugin/<ARTIFACT_ID>/src/main/resources/devicetypes` directory.
4.  Once you are done with writing the plugin, UI, APIs and Analytics for the device, you need to build `<CARBON_DEVICE_MGT_PLUGINS>`.
5.  Clone Entgra IoT Server if you have not done previously and build it.

    
    git clone https://github.com/wso2/product-iots.git
    cd <PRODUCT_IOTS>
    mvn clean install
    

6.  An OSGI bundle for the device type is created in Entgra CDMF after reading the `device.xml` file.   

Now you are done with creating the device plugin. It's a very easy with Entgra IoT Server. Let's look at an already implemented scenario to understand this concept.

### Sample - writing device plugins

for this guide let's take a look at the already implemented Android plugin device type:



Take a look at the configured device type XML file, before you begin


<?xml version="1.0" encoding="utf-8"?>
<!--
  ~ Copyright (c) 2016, WSO2 Inc. (http://www.wso2.org) All Rights Reserved.
  ~
  ~ WSO2 Inc. licenses this file to you under the Apache License,
  ~ Version 2.0 (the "License"); you may not use this file except
  ~ in compliance with the License.
  ~ You may obtain a copy of the License at
  ~
  ~ http://www.apache.org/licenses/LICENSE-2.0
  ~
  ~ Unless required by applicable law or agreed to in writing,
  ~ software distributed under the License is distributed on an
  ~ "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
  ~ KIND, either express or implied. See the License for the
  ~ specific language governing permissions and limitations
  ~ under the License.
  -->
<DeviceTypeConfiguration name="android">
    <DeviceDetails table-id="AD_DEVICE"/>
    <License>
        <Language>en_US</Language>
        <Version>1.0.0</Version>
        <Text>This is license text</Text>
    </License>
    <ProvisioningConfig>
        <SharedWithAllTenants>false</SharedWithAllTenants>
    </ProvisioningConfig>
	<!--
    isScheduled element used to enable scheduler task to send push notification.
    Task will send push notification as batches. So this will reduce sudden request burst when many devices try to
    access server after receiving push notification.
    -->
    <!--Configuration for enable firebase push notifications-->
    <!--<PushNotificationProviderConfig type="FCM" isScheduled="false">-->
    <!--</PushNotificationProviderConfig>-->
    <DataSource>
        <jndiConfig>
            <name>jdbc/MobileAndroidDM_DS</name>
        </jndiConfig>
        <tableConfig>
            <Table name="AD_DEVICE">
                <PrimaryKey>DEVICE_ID</PrimaryKey>
                <Attributes>
                    <Attribute>GCM_TOKEN</Attribute>
                    <Attribute>DEVICE_INFO</Attribute>
                    <Attribute>IMEI</Attribute>
                    <Attribute>IMSI</Attribute>
                    <Attribute>OS_VERSION</Attribute>
                    <Attribute>DEVICE_MODEL</Attribute>
                    <Attribute>VENDOR</Attribute>
                    <Attribute>LATITUDE</Attribute>
                    <Attribute>LONGITUDE</Attribute>
                    <Attribute>SERIAL</Attribute>
                    <Attribute>MAC_ADDRESS</Attribute>
                    <Attribute>DEVICE_NAME</Attribute>
                    <Attribute>OS_BUILD_DATE</Attribute>
                </Attributes>
            </Table>
        </tableConfig>
    </DataSource>
    <Features>
        <Feature code="DEVICE_RING">
            <Name>Ring</Name>
            <Description>Ring the device</Description>
            <Operation context="/api/device-mgt/android/v1.0/admin/devices/ring" method="POST" type="application/json"></Operation>
        </Feature>
        <Feature code="DEVICE_LOCK">
            <Name>Device Lock</Name>
            <Description>Lock the device</Description>
            <Operation context="/api/device-mgt/android/v1.0/admin/devices/lock-devices" method="POST" type="application/json"></Operation>
        </Feature>
        <Feature code="DEVICE_LOCATION">
            <Name>Location</Name>
            <Description>Request coordinates of device location</Description>
            <Operation context="/api/device-mgt/android/v1.0/admin/devices/location" method="POST" type="application/json"></Operation>
        </Feature>
        <Feature code="CLEAR_PASSWORD">
            <Name>Clear Password</Name>
            <Description>Clear current password</Description>
            <Operation context="/api/device-mgt/android/v1.0/admin/devices/clear-password" method="POST" type="application/json"></Operation>
        </Feature>
        <Feature code="DEVICE_REBOOT">
            <Name>Reboot</Name>
            <Description>Reboot the device</Description>
            <Operation context="/api/device-mgt/android/v1.0/admin/devices/reboot" method="POST" type="application/json"></Operation>
        </Feature>
        <Feature code="UPGRADE_FIRMWARE">
            <Name>Upgrade Firmware</Name>
            <Description>Upgrade Firmware</Description>
            <Operation context="/api/device-mgt/android/v1.0/admin/devices/upgrade-firmware" method="POST" type="application/json"></Operation>
        </Feature>
        <Feature code="DEVICE_MUTE">
            <Name>Mute</Name>
            <Description>Enable mute in the device</Description>
            <Operation context="/api/device-mgt/android/v1.0/admin/devices/mute" method="POST" type="application/json"></Operation>
        </Feature>
        <Feature code="NOTIFICATION">
            <Name>Message</Name>
            <Description>Send message</Description>
            <Operation context="/api/device-mgt/android/v1.0/admin/devices/send-notification" method="POST" type="application/json"></Operation>
        </Feature>
        <Feature code="CHANGE_LOCK_CODE">
            <Name>Change Lock-code</Name>
            <Description>Change current lock code</Description>
            <Operation context="/api/device-mgt/android/v1.0/admin/devices/change-lock-code" method="POST" type="application/json"></Operation>
        </Feature>
        <Feature code="ENTERPRISE_WIPE">
            <Name>Enterprise Wipe</Name>
            <Description>Remove enterprise applications</Description>
            <Operation context="/api/device-mgt/android/v1.0/admin/devices/enterprise-wipe" method="POST" type="application/json"></Operation>
        </Feature>
        <Feature code="WIPE_DATA">
            <Name>Wipe Data</Name>
            <Description>Factory reset the device</Description>
            <Operation context="/api/device-mgt/android/v1.0/admin/devices/wipe" method="POST" type="application/json"></Operation>
        </Feature>
        <Feature code="WIFI">
            <Name>Wifi</Name>
            <Description>Setting up wifi configuration</Description>
        </Feature>
        <Feature code="CAMERA">
            <Name>Camera</Name>
            <Description>Enable or disable camera</Description>
        </Feature>
    </Features>
	<TaskConfiguration> //Name needs to be decided <!--DeviceMonitoring-->
       <Enable>true</Enable>
       <Frequency>60000</Frequency> //Name needs to be decided <!--Period-->
       <Operations>
           <Operation>
               <Name>DEVICE_INFO</Name>
               <RecurrentTimes>1</RecurrentTimes> //Name needs to be decided <!--PeriodFactor-->
           </Operation>
           <Operation>
               <Name>APPLICATION_LIST</Name>
               <RecurrentTimes>5</RecurrentTimes>
           </Operation>
           <Operation>
               <Name>DEVICE_LOCATION</Name>
               <RecurrentTimes>1</RecurrentTimes>
           </Operation>
       </Operations>
   </TaskConfiguration>
	<PolicyMonitoring enabled="true"/>
	<InitialOperationConfig>
        <Operations>
            <Operation>DEVICE_INFO</Operation>
            <Operation>APPLICATION_LIST</Operation>
            <Operation>DEVICE_LOCATION</Operation>
        </Operations>
    </InitialOperationConfig>
    <DeviceStatusTaskConfig>
        <RequireStatusMonitoring>true</RequireStatusMonitoring>
        <Frequency>300</Frequency>
        <IdleTimeToMarkInactive>900</IdleTimeToMarkInactive>
        <IdleTimeToMarkUnreachable>600</IdleTimeToMarkUnreachable>
    </DeviceStatusTaskConfig>
</DeviceTypeConfiguration>


To get a clear understanding of how you need to configure your device type, let's go through the properties of the XML file:

<table>
  <colgroup>
    <col>
    <col>
  </colgroup>
  <tbody>
    <tr>
      <th>Property</th>
      <th>Description</th>
    </tr>
    <tr>
      <td>
        <p><strong><code>DeviceTypeConfiguration</code></strong></p>
      </td>
      <td>
        
          <p>Define the name of the device type. The name of the device you are creating will be mapped with the APIs, UIsand other elements when creating the device plugin.<br>Example:</p>
          
            
              
            
          
        
      </td>
    </tr>
    <tr>
      <td>
        <p><strong><code>DeviceDetails</code></strong></p>
      </td>
      <td>
        
          <p>Every device type will need to store device specific data. Therefore, define the table ID of the database to which you are storing the data in this property.<br>This is the table we created in the <code>dbbdscripts</code> folder when <a href="https://entgra-documentation.gitlab.io/v3.7.0/docs/using-entgra-iot-server/device-manufacturer-guide/extending-entgra-iot-server/#writing-the-device-feature" data-linked-resource-id="352821995" data-linked-resource-version="1" data-linked-resource-type="page">writing the device feature</a>.<br>Example:</p>
 
      </td>
    </tr>
    <tr>
      <td>
        <p><strong><code>License</code></strong></p>
      </td>
      <td>
        
          <p>Add the license agreement details under this property. If you are allowing third party users to use the device type you are creating, then this will be very useful to make sure they agree with the terms you mention.<br>Example:</p>
          
            
              &lt;License&gt;
    &lt;Language&gt;en_US&lt;/Language&gt;
    &lt;Version&gt;1.0.0&lt;/Version&gt;
    &lt;Text&gt;This is license text&lt;/Text&gt;
&lt;/License&gt;
      
      </td>
    </tr>
    <tr>
      <td>
        
      </td>
      <td>
        
          <p>Your device type can be accessed by many tenants or only the super tenant that is created in Entgra IoTS by default. Tenant management for a device type is configured within this property.</p>
          <ul>
            <li>
              <p>If you define <code>false</code> as the value for <code>SharedWithAllTenants</code>, the device you are creating will only be accessible by the super tenant of Entgra IoTS.</p>
              
                
                  &lt;ProvisioningConfig&gt;
    &lt;SharedWithAllTenants&gt;false&lt;/SharedWithAllTenants&gt;
&lt;/ProvisioningConfig&gt;
                
              
            </li>
            <li>
              <p>If you define <code>true</code> as the value for <span><span><code>SharedWithAllTenants</code>, the device you are creating can be accessed by all the tenants created in Entgra IoTS.<br></span></span></p>
              
                
                  &lt;ProvisioningConfig&gt;
    &lt;SharedWithAllTenants&gt;true&lt;/SharedWithAllTenants&gt;
&lt;/ProvisioningConfig&gt;
                
              
            </li>
          </ul>
        
      </td>
    </tr>
    <tr>
      <td>
        <p><strong><code>PushNotificationProviderConfig</code></strong></p>
      </td>
      <td>
        <p>Configure the device type to run the scheduled push notification&nbsp;task. This property is disabled by default. For more information on scheduling the push notification, see <a href="https://entgra-documentation.gitlab.io/v3.7.0/docs/using-entgra-iot-server/product-administration/Scheduling-the-Push-Notification-Task/" data-linked-resource-id="352821772" data-linked-resource-version="1" data-linked-resource-type="page">Scheduling the Push Notification Task</a>.</p>
      </td>
    </tr>
    <tr>
      <td>&nbsp;<strong><code>DataSource</code></strong></td>
      <td>
        
          <p>The device you create can have attributes specific to the device and you will need to save the data in Entgra IoTS. This property lets you define the database and the respective table in the database where you will be saving the device specific details.</p>
          
            <p class="title">Note</p><span class="aui-icon aui-icon-small aui-iconfont-warning confluence-information-macro-icon"></span>
            
              <p>The <code>ATTRIBUTE</code> property must contain the exact name of the database column. Else when the server is adding the device details to the configured table and the names don't match, you will run into errors.</p>
              <p><span>The attribute details were the column names of the table you created in the </span><a href="/doc/en/lb2/Writing-the-Device-Feature.html#WritingtheDeviceFeature-DBscriptsdirectory" data-linked-resource-id="352821995" data-linked-resource-version="1" data-linked-resource-type="page"><code>dbbdscripts</code><span> folder</span></a><span>&nbsp;</span>and the datasource details were configured in the <a href="/doc/en/lb2/Writing-the-Device-Feature.html#WritingtheDeviceFeature-Datasourcesdirectory" data-linked-resource-id="352821995" data-linked-resource-version="1" data-linked-resource-type="page"><code>datasource</code> directory</a> <span>when </span>writing the device feature.</p>
            
          
          <p>In the android plugin sample, we add the data to the <code>AD_DEVICE</code> table, and the <code>MobileAndroidDM_DS</code> datasource.</p>
          
            
              &lt;DataSource&gt;
    &lt;jndiConfig&gt;
        &lt;name&gt;jdbc/MobileAndroidDM_DS&lt;/name&gt;
    &lt;/jndiConfig&gt;
    &lt;tableConfig&gt;
        &lt;Table name="AD_DEVICE"&gt;
            &lt;PrimaryKey&gt;DEVICE_ID&lt;/PrimaryKey&gt;
            &lt;Attributes&gt;
                &lt;Attribute&gt;GCM_TOKEN&lt;/Attribute&gt;
                &lt;Attribute&gt;DEVICE_INFO&lt;/Attribute&gt;
                &lt;Attribute&gt;IMEI&lt;/Attribute&gt;
                &lt;Attribute&gt;IMSI&lt;/Attribute&gt;
                &lt;Attribute&gt;OS_VERSION&lt;/Attribute&gt;
                &lt;Attribute&gt;DEVICE_MODEL&lt;/Attribute&gt;
                &lt;Attribute&gt;VENDOR&lt;/Attribute&gt;
                &lt;Attribute&gt;LATITUDE&lt;/Attribute&gt;
                &lt;Attribute&gt;LONGITUDE&lt;/Attribute&gt;
                &lt;Attribute&gt;SERIAL&lt;/Attribute&gt;
                &lt;Attribute&gt;MAC_ADDRESS&lt;/Attribute&gt;
                &lt;Attribute&gt;DEVICE_NAME&lt;/Attribute&gt;
                &lt;Attribute&gt;OS_BUILD_DATE&lt;/Attribute&gt;
            &lt;/Attributes&gt;
        &lt;/Table&gt;
    &lt;/tableConfig&gt;
&lt;/DataSource&gt;
   
      </td>
    </tr>
    <tr>
      <td><strong><code>Features</code></strong></td>
      <td>
        
          <p>Your device type will carry out various operations. For example, an Android device needs to be locked and unlocked, the device location needs to be fetched and much more. Therefore, the feature management aspect of the device is handled by this property.</p>
          <p>You can list out all the features you wish for your device to have using the sub-properties listed below:</p>
          <ul>
            <li>
              <p><code>Feature code</code>: This is used to uniquely identify the operation of a device.<br>Moreover, the UI elements of each operation are mapped to the <code>cdmf.unit.device.type.&lt;DEVICE_TYPE&gt;.tyoe-view/public.config.json</code> file via this feature code. For more information, see <a href="/doc/en/lb2/Writing-UI-Extensions.html" data-linked-resource-id="352822195" data-linked-resource-version="1" data-linked-resource-type="page">Writing UI Extensions</a>.<br>Example: <code><a href="https://github.com/wso2/carbon-device-mgt-plugins/blob/master/components/mobile-plugins/android-plugin/org.wso2.carbon.device.mgt.mobile.android.ui/src/main/resources/jaggeryapps/devicemgt/app/units/cdmf.unit.device.type.android.type-view/private/config.json" class="external-link" rel="nofollow">cdmf.unit.device.type.android.type-view/public/config.json</a></code></p>
    
            </li>
            <li>
              <p><strong><code>Name</code></strong>: Define the name you want the operation to be identified.</p>
  
            </li>
            <li>
              <p><span style="font-family: monospace;"><strong>Description</strong></span>: Provide details as to what the operation will do when it's carried out.</p>
              
            </li>
            <li>
              <p><strong><code>Operation</code></strong>: Define the API path that will trigger the operation. The operation you define here will be then matched with the API you create for the operation by mapping the resource path and HTTP method provided. To know more, see <a href="https://entgra-documentation.gitlab.io/v3.7.0/docs/using-entgra-iot-server/device-manufacturer-guide/extending-entgra-iot-server/#writing-device-apis" data-linked-resource-id="352822106" data-linked-resource-version="1" data-linked-resource-type="page">Writing Device APIs</a>.</p>
              
                <p class="title">Note</p><span class="aui-icon aui-icon-small aui-iconfont-warning confluence-information-macro-icon"></span>
                
                  <p>If you don't define the operation property for a feature, that feature will not be shown as an operation in the device type user interface.</p>
               
              <p><br></p>
              <ul>
                <li><code>Context</code>: Define the API path</li>
                <li><code>method</code>: Define the HTTP method od the operation.</li>
                <li><code>type</code>: Define the content-type.</li>
              </ul>
            </li>
          </ul>
        
      </td>
    </tr>
    <tr>
      <td><strong><code>PushNotificationProvider</code></strong></td>
      <td>
        
          <p>This property defines the transport over which the device and the server communicates. If the file based property is configured, Entgra IoTS will read this file and communicate with the device based on the configured transport sender mechanism. If this property is not configured in the device type XML file, Entgra IoTS will get the required details from platform configurations and you will need to configure the registry accordingly.</p>
          <p>Defining this property is useful when configuring the device type to communicate over many transport senders.<br><br>The Android plugin does not have this property defined as the transport sender method is configured under the platform configuration of the device. But let's look at a sample configuration:</p>
          
            
              &lt;PushNotificationProvider type="MQTT"&gt;
    &lt;FileBasedProperties&gt;true&lt;/FileBasedProperties&gt;
    &lt;!--if file based properties is set to false then the configuration will be picked from platform configuration--&gt;
    &lt;ConfigProperties&gt;
        &lt;Property Name="mqtt.adapter.name"&gt;android.mqtt.adapter&lt;/Property&gt;
        &lt;Property Name="username"&gt;admin&lt;/Property&gt;
		&lt;Property Name="password"&gt;admin&lt;/Property&gt;
        &lt;Property Name="qos"&gt;0&lt;/Property&gt;
        &lt;Property Name="clearSession"&gt;true&lt;/Property&gt;
    &lt;/ConfigProperties&gt;
&lt;/PushNotificationProvider&gt;
            
          
        
      </td>
    </tr>
    <tr>
      <td><strong><code>TaskConfiguration</code></strong></td>
      <td>
        
          <p>This property defines the operation types that need to be triggered from the server.</p>
          
            
              &lt;TaskConfiguration&gt; 
    &lt;Enable&gt;true&lt;/Enable&gt;
    &lt;Frequency&gt;60000&lt;/Frequency&gt; //Name needs to be decided &lt;!--Period--&gt;
    &lt;Operations&gt;
        &lt;Operation&gt;
            &lt;Name&gt;DEVICE_INFO&lt;/Name&gt;
            &lt;RecurrentTimes&gt;1&lt;/RecurrentTimes&gt; //Name needs to be decided &lt;!--PeriodFactor--&gt;
        &lt;/Operation&gt;
        &lt;Operation&gt;
            &lt;Name&gt;APPLICATION_LIST&lt;/Name&gt;
            &lt;RecurrentTimes&gt;5&lt;/RecurrentTimes&gt;
        &lt;/Operation&gt;
        &lt;Operation&gt;
            &lt;Name&gt;DEVICE_LOCATION&lt;/Name&gt;
            &lt;RecurrentTimes&gt;1&lt;/RecurrentTimes&gt;
        &lt;/Operation&gt;
    &lt;/Operations&gt;
&lt;/TaskConfiguration&gt;
            
          
        
      </td>
    </tr>
    <tr>
      <td><strong><code>PolicyMonitoring</code></strong></td>
      <td>
        
          <p>This property defines whether the device types requires policy compliance monitoring.</p>
          
            
              
            
          
        
      </td>
    </tr>
    <tr>
      <td><strong><code>IntialOperationConfig</code></strong></td>
      <td>Define the operations that need to run soon after a device is enrolled with Entgra IoT Server.</td>
    </tr>
    <tr>
      <td>
        <p><code><strong>DeviceStatusTaskConfig</strong></code></p>
      </td>
      <td>
        <p>Due to various reasons, the devices registered with Entgra IoT Server might not be able to communicate with the server continuously.&nbsp;When the device is not actively communicating with the server, you need to know of it to take necessary actions, such as checking if the device has any malfunctions and repairing it or checking if the device was stolen. For more information on configuring the device monitoring status, see <a href="https://entgra-documentation.gitlab.io/v3.7.0/docs/using-entgra-iot-server/product-administration/Monitoring-the-Device-Status/" data-linked-resource-id="352821737" data-linked-resource-version="1" data-linked-resource-type="page">Monitoring the Device Status</a>.</p>
        <ul>
          <li>
            <p><strong><code>RequireStatusMonitoring</code></strong>: If the value is set to true, it enables the status monitoring task for the device type. Else it will not monitor the status of the device type.</p>
          </li>
          <li>
            <p><strong><code>Frequency</code></strong>: Define how often this task should run for the specified device type. The value needs to be given in seconds.</p>
            
              <p>Make sure to have the value above 60 seconds.</p>
            
          </li>
          <li>
            <p><strong><code>IdleTimeToMarkUnreachable</code></strong>: Define after how long the device needs to be marked as unreachable. The value needs to be given in seconds.</p>
          </li>
          <li>
            <p><strong><code>IdleTimeToMarkInactive</code></strong>: Define after how long the device needs to be marked as inactive. The value needs to be given in seconds.</p>
          </li>
        </ul>
      </td>
    </tr>
  </tbody>
</table>

## Writing Device Plugins via Java Code

When writing a device type using the Java Code approach you need to write the device plugin, APIs, transports, UI, and analytics components from scratch. This section describes how to write the device plugin.



Why do we need a device plugin?









A device plugin is an OSGi bundle that gets wired with the Entgra Connected Device Management Framework (CDMF). When wrting a device type using the Java code approach the device requires a specific device plugin.  A specific device plugin is required due to the following reasons:

*   Enables its the device manufacturer or the device type creators to have control over the device.
*   A device can have its own unique set of attributes. Therefore it is necessary to maintain a separate data store to keep track of the data. This can be achieved by maintaining a separate data store for each device in the plugin layer.

![image](352822100.png)











Before you begin



[Make sure you have the required folder structure, and the databases and datasources configured](/doc/en/lb2/Writing-the-Device-Feature.html).





To understand the device plugin implementation let's take a look at the Raspberry Pi device implementation. Follow the steps given below to create a new device plugin:

1.  Implement an interface for Raspberry Pi. For more information, see the [implementation of `DeviceManagerService` for Raspberry Pi](https://github.com/wso2/product-iots/blob/master/modules/distribution/src/core/samples/sampledevice/component/plugin/src/main/java/org.wso2.carbon/sampledevice/plugin/impl/DeviceTypeManagerService.java).

    

    Why is this step required?

    

    Entgra IoT Server can have many device types. These device types will be registered on Entgra CDMF when the OSGI runtime starts. For the new device type to be registered with the CDMF, it needs to have an interface that the OSGI identifies at its runtime. The `DeviceManagementService` interface is implemented for this reason. Therefore, it is important that you implement the `DeviceManagementService` interface extended from 
    org.wso2.carbon.device.mgt.common.spi  
    .
     `DeviceManagementService`. You are able to implement methods unique to a device by overriding the methods defined in the interface.

    

    

    The `DeviceManagementService` interface is shwon below.

    
    public interface DeviceManagementService {
        void init() throws DeviceManagementException;
        String getType();
        OperationMonitoringTaskConfig getOperationMonitoringConfig();
        DeviceManager getDeviceManager();
        ApplicationManager getApplicationManager();
        ProvisioningConfig getProvisioningConfig();
        PushNotificationConfig getPushNotificationConfig();
        PolicyMonitoringManager getPolicyMonitoringManager();
    }
    

    

    

    

    

    *   **`getType()`**  
        Retrieves the name of the device-type.

    *   **`init()`**  
        The custom initialization implementations need to be included here.

    *   **`getOperationMonitoringConfig()`**    
        Returns an object, which is an implementation of the `org.wso2.carbon.device.mgt.common.OperationMonitoringTaskConfig` interface.

    *   **`getDeviceManager()`**  
        Returns an object, which is an implementation of the interface `org.wso2.carbon.device.mgt.common.DeviceManager`.

    *   **`getApplicationManager()`**  
        Returns an object, which is an implementation of the `org.wso2.carbon.device.mgt.common.app.mgt.ApplicationManager` interface. 

    *   **`getProvisioningConfig()`**  
        Returns the provisioning details, which includes the name of the tenant domain the device-type needs to be registered to and indicates whether the device-type is to be shared with all the tenants.  
        `true` represents that the device-type should be visible to all tenants and `false` represents that it is not visible to other tenants.  

    *   **`getPolicyMonitoringManager()`**    
        Returns an object, which is an implementation of the `org.wso2.carbon.device.mgt.common.policy.mgt.PolicyMonitoringManager` interface.

    

    

    

2.  Implement the `DeviceManager` interface. For more information, see [the implementation of `DeviceManager` for Raspberry Pi](https://github.com/wso2/product-iots/blob/master/modules/distribution/src/core/samples/sampledevice/component/plugin/src/main/java/org.wso2.carbon/sampledevice/plugin/impl/DeviceTypeManager.java).

    

    Why is this step required?

    

    Implement the interface `DeviceManager` for Raspberry Pi via the `org.wso2.carbon.device.mgt.common.DeviceManager` in order to implement the `getDeviceManager()`method that is shown in step 1\. The `DeviceManager` interface will be used for enrolling, disenrolling, activating and deactivating a device. 

    

    

3.  Create a Database Access Object (DAO) on the created `DeviceManager` interface to manage data source connections.

4.  Register as an OSGI service.  
    Example:

    `ServiceRegistration raspberrypiServiceRegRef = bundleContext.registerService(DeviceManagementService.class.getName(), new RaspberrypiManagerService(), null);`

    

    

    `RaspberrypiManagerService` is the implementation of the interface showed in step 1.

    

    

5.  Start the IoT Server.

    
    cd <IoTS_HOME>
    ./wso2server.sh
    

### How it works

The following section describes how a Raspberry Pi is enrolled using a created device plugin:

*   The device enrollment calls will be passed through the `DeviceManager` implementation in the `DeviceManagerService`.

*   The `DeviceManager` implementation implements the interface `org.wso2.carbon.device.mgt.common.DeviceManager`.
*   The implemented interface manages the data of the Raspberry Pi, such as information related to enrollment, status, ownership, claimable, license and tenant configuration. 
*   The implementation needs to be included in an OSGI bundle. Once the bundle is activated, the device will be registered on the Connected Device Management Framework (CDMF).


# Writing Device APIs

The following subsections explain how to write and secure device APIs:

1.  Create a JAXRS web application for APIs. 

    

    

    For more information, see the [JAXRS implementation for APIs in Raspberry Pi](https://github.com/wso2/product-iots/tree/master/modules/distribution/src/core/samples/sampledevice/component/api/src/main).

    

    

2.  Annotate the web app with the name and context, so that all the APIs of a device are grouped and can be identified instantly.  
    Example: All the APIs will be grouped under raspberryPi

    
    @SwaggerDefinition(
            info = @Info(
                    version = "1.0.0",
                    title = "",
                    extensions = {
                            @Extension(properties = {
                                    @ExtensionProperty(name = "name", value = "raspberrypi"),
                                    @ExtensionProperty(name = "context", value = "/raspberrypi"),
                            })
                    }
            ),
            tags = {
                    @Tag(name = "raspberrypi", description = "")
            }
    

3.  Annotate the APIs using the swagger annotations.  For more information on swagger annotations, see [Annotations-1.5.X](https://github.com/swagger-api/swagger-core/wiki/Annotations-1.5.X).   
    Example:

    
    @Path("device/{deviceId}/bulb")
    @POST
    @Scope(key = "device:raspberrypi:enroll", name = "", description = "")
    Response switchBulb(@PathParam("deviceId") String deviceId, @QueryParam("state") String state);
    

4.  The resources used by external entities can be secured with WSO2 API Manager by including specific XML elements to the `web.xml` file of the web application that implements the APIs.

    

    

    

    

    

    
    <?xml version="1.0" encoding="utf-8"?>
    <web-app version="2.5"
        xmlns="http://java.sun.com/xml/ns/javaee"
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
             xsi:schemaLocation="http://java.sun.com/xml/ns/javaee http://java.sun.com/xml/ns/javaee/web-app_2_5.xsd"
             metadata-complete="true">
        <display-name>RaspberryPi</display-name>
        <description>RaspberryPi</description>
        <servlet>
            <servlet-name>CXFServlet</servlet-name>
            <servlet-class>org.apache.cxf.transport.servlet.CXFServlet</servlet-class>
            <load-on-startup>1</load-on-startup>
        </servlet>
        <servlet-mapping>
            <servlet-name>CXFServlet</servlet-name>
            <url-pattern>/*</url-pattern>
        </servlet-mapping>
        <context-param>
            <param-name>doAuthentication</param-name>
            <param-value>true</param-value>
        </context-param>
        <context-param>
            <param-name>isSharedWithAllTenants</param-name>
            <param-value>true</param-value>
        </context-param>
        <!--publish to apim-->
        <context-param>
            <param-name>managed-api-enabled</param-name>
            <param-value>true</param-value>
        </context-param>
    </web-app>
    

    JAXRS web applications are used to create and configure APIs. By default, a web application has a `web.xml` file. Entgra IoTS secures the APIs through `web.xml` by configuring it as shown below:

    <table>
      <colgroup>
        <col>
        <col>
      </colgroup>
      <tbody>
        <tr>
          <th>XML Property</th>
          <th>Description</th>
        </tr>
        <tr>
          <td>
            
          </td>
          <td>APIs can be unauthenticated or authenticated where each API header will be validated to see if it meets the required conditions. If it's configured as true, the API is authenticated, and if it's configured as false, the API is unauthenticated.</td>
        </tr>
        <tr>
          <td>
            <p><code>isSharedWithAllTenants</code></p>
          </td>
          <td>
            
              <p>Optional. If this tag is included in the <code>web.xml</code> file and is configured as true, it indicates that the APIs are shared with all the tenants. If it's configured as false, it indicates that the APIs are restricted to the tenant that created them.</p>
              
                
                  <p>If this tag is not present in the <code>web.xml</code> file, the default action would be to share the APIs with all the tenants.</p>
                
              
            
          </td>
        </tr>
        <tr>
          <td>
            
          </td>
          <td>
            
              <ul>
                <li>
                  <p><code>true</code>&nbsp;- The APIs in the respective JAXRS web application are secured via the API Manager. There will be a token issued to secure the API each time the API is called via the API Manager gateway.</p>
                  
                    
                      <p>If managed-api-enabled is true, the APIs in the web application are identified as controller APIs in the context of Entgra IoTS.</p>
                    
                  
                </li>
              </ul>
              <ul>
                <li>
                  <p><code>false</code> - &nbsp;The APIs in the respective JAXRS web application is not secured via the API Manager.</p>
                  
                    
                      <p>If managed-api-enabled is false, the APIs in the web application are identified as device manager APIs in the context of Entgra IoTS.</p>
                    
                  
                </li>
              </ul>
              <p>For example of setting <code>managed-api-enabled</code> to false:</p>
              
                
                  &lt;context-param&gt;
       &lt;param-name&gt;managed-api-enabled&lt;/param-name&gt;
       &lt;param-value&gt;false&lt;/param-value&gt;
    &lt;/context-param&gt;
                
              
            
          </td>
        </tr>
      </tbody>
    </table>

# Writing Transport Extensions

    The following subsections provides more information on how the server and the device communicate with each other via Entgra IoTS transport extensions:

*   [Server Communicating with the Device](/doc/en/lb2/Server-Communicating-with-the-Device.html)
*   [Device Communicating with the Server](/doc/en/lb2/Device-Communicating-with-the-Server.html)
*   [Adding a Push Notification Provider](/doc/en/lb2/Adding-a-Push-Notification-Provider.html)

## Server Communicating with the Device

When you send a command to a device, it goes through the Entgra IoT Server transport sender mechanism. For example, switching on or switching off the bulb on the RaspberryPi device type. To identify the flow of what happens when Entgra IoT Server receives the command, and how it communicates take a look at the diagram given below:

![image](352822146.png)

Let's understand this clearly by using the RaspberryPi device type:

1.  Write the API to switch the bulb on. Once Entgra IoT Server receives the command it will call the API you have written to switch the bulb on.

    

    

    For more more information, see [Writing Device APIs](https://entgra-documentation.gitlab.io/v3.7.0/docs/using-entgra-iot-server/device-manufacturer-guide/extending-entgra-iot-server/#writing-device-apis).

    

    

2.  Define the operation type for the API in the `<DEVICE_TYPE>SeviceImpl.java` file. Entgra IoT Server supports the following 4 operation types.

    *   `POLICY`
    *   `COMMAND`
    *   `CONFIG`
    *   `PROFILE`

    Example: The [`RaspberryPiServiceImpl.java`](https://github.com/wso2/product-iots/blob/master/modules/distribution/src/core/samples/sampledevice/component/api/src/main/java/org.wso2.carbon/sampledevice/api/DeviceTypeServiceImpl.java#L133-L137) file has configured the bulb switching on and off API as a `COMMAND` operation.

    
    Operation commandOp = new CommandOperation();
    commandOp.setCode("bulb");
    commandOp.setType(Operation.Type.COMMAND);
    commandOp.setEnabled(true);
    commandOp.setPayLoad(actualMessage);
    

3.  Assign the properties you defined in step 2 above to a publisher topic as done in the [`RaspberryPiServiceImpl.java`](https://github.com/wso2/carbon-device-mgt-plugins/blob/v4.0.55/components/device-types/raspberrypi-plugin/org.wso2.carbon.device.mgt.iot.raspberrypi.api/src/main/java/org/wso2/carbon/device/mgt/iot/raspberrypi/service/impl/RaspberryPiServiceImpl.java#L84) file.

    
    Properties props = new Properties();
    props.setProperty(RaspberrypiConstants.MQTT_ADAPTER_TOPIC_PROPERTY_NAME, publishTopic);
    commandOp.setProperties(props);
    

4.  The command is then sent to the device.  
    Follow the steps given below to find out if the device received the command: 

    1.  Sign in to the Entgra IoT Server device management console.

        

        

        

        

        Follow the instructions given below to start Entgra IoT Server, and sign into the device management console:

        1.  Navigate to the Entgra IoT Server pack's samples directory and run the `device-plugins-deployer.xml` file.

            

            Why is this needed?

            

            Before enrolling devices with Entgra IoT Server you need to have the device type plugins created. The [Android](https://entgra-documentation.gitlab.io/v3.7.0/docs/tutorials/android/android.html), [Android Sense](https://entgra.atlassian.net/wiki/spaces/IoTS370/pages/352816866/Android+Sense), [Windows](https://entgra.atlassian.net/wiki/spaces/IoTS370/pages/352818773/Windows), and [Android Virtual Device](https://entgra.atlassian.net/wiki/spaces/IoTS370/pages/352817149/Android+Virtual+Device) sample device type plugins are available by default on Entgra IoT Server. You need to run the command given below to create the [Raspberry Pi](https://entgra.atlassian.net/wiki/spaces/IoTS370/pages/352818675/Raspberry+Pi), [Arduino](https://entgra.atlassian.net/wiki/spaces/IoTS370/pages/352817394/Arduino), and [Virtual Fire Alarm](Enterprise-IoT-solution_352814351.html) sample plugins.


            For more information on writing your own device plugin, see [Writing Device Types](https://entgra-documentation.gitlab.io/v3.7.0/docs/device-manufacturer-guide/extending-entgra-iot-server.html).


            

            

            Example:

            
            cd <IOTS_HOME>/samples
            mvn clean install -f device-plugins-deployer.xml
            

        2.  Start Entgra IoT Server by starting the three profiles in the following order:

            1.  Start the broker profile, which corresponds to the WSO2 Message Broker profile.

                The default port assigned for the broker is 9446.

            2.  Start the core profile, which corresponds to the WSO2 Connected Device Management Framework (WSO2 CDMF) profile.

                The default port assigned for the core is 9443.

            3.  Start the analytics profile, which corresponds to the WSO2 Data Analytics Server profile.

                The default port assigned for analytics is 9445.

        3.  Access the device management console by navigating to `https://<HTTPS_HOST>:<HTTPS_PORT>/devicemgt`.  
            Example: `https://localhost:9443/devicemgt`

        4.  Enter your username and password.  
            If you are new to using Entgra IoT Server, you need to first register before you can to log into the WSO2 device management console.

            

            

            

            

            Follow the instructions given below to register with Entgra IoT Server:

            1.  Click **Create an account**  
                ![image](352814446.png)
            2.  Fill out the registration form.

                ![image](352814440.png)

                *   First Name: Provide your first name.
                *   Last Name: Provide your last name.
                *   Username: Provide a username. It should be at least 3 characters long with no white spaces.
                *   Email: Provide a valid email address.
                *   Password: Provide a password. It should be at least 8 characters long.
                *   Confirm Password: Provide the password again to confirm it.
            3.  Click **Register**.

            

            

            

        5.  Click **LOGIN**. 

        

        

        

    2.  Click the device you sent the command.   
        Example: Click virtual fire alarm. 
    3.  Click Operation logs, to view the status of the command you sent.

        

        

        The operation status can be any of the following values:

        *   `IN-PROGRESS` - The operation is processing on the IoTS server side and has not yet been delivered to the device.
        *   `PENDING` - The operation is delivered to the device but the response from the device is pending.
        *   `COMPLETED` - The operation is delivered to the device and the server has received a response back from the device.
        *   `ERROR` - An error has occurred while carrying out the operation.

        

        

        ![image](352822152.png)

## Device Communicating with the Server

 The data received by your device sensors will be sent to WSO2 Data Analytic Server (DAS) so that you can view them in real-time or as historical data. Before sending data to WSO2 DAS, you will want to encode it or convert it to a preferred format. How can you do this?

Entgra IoT Server provides you with the ability to transform and validate content before sending them to WSO2 DAS. Once this is done you can configure Entgra IoT Server to send data to subscribed events or send them directly to WSO2 DAS. Take a look at the diagram given below:

![image](352822178.png)

Let's take a look at how you can send data to WSO2 DAS:

You can write your own custom logic to transform and validate content before sending your data to WSO2 DAS. To understand how this is done, let's take a look at how the content transformer and validator are used by the analytics receiver of the RaspberryPi device type





The MQTT transport extension is used for this purpose.





1.  Transform the data you receive, such as encoding the data, before sending it to WSO2 DAS. 

    

    

    In the case of the RaspberryPi device type, it uses the default content transformer that sends the data as it is to WSO2 DAS.

    

    

    1.  If you wish to transform the content, you need to implement the `contentTransformer` interface.  
        Example:

        `public class MQTTContentTransformer implements ContentTransformer {}`

    2.  Customize the content transformer to match your requirement.
2.  Validate content.

    

    Why validate?

    

    Once the MQTT transport extension/input adapter receives the data sent by the device, you will need to verify if it's a spoofer or if it's the device user that is sending the data. After this fact is verified you can configure Entgra IoT Server to send the content to WSO2 DAS.  

    In the RaspberryPi device type, the API path that is used to send the data is verified by checking if the device ID in it matches the ID of the device registered with Entgra IoT Server. You can validate the content using the MQTT, HTTP, and XMPP content validators.

    

    

    1.  Extend the content transformer interface, and customize it to suit our requirement.  
        Example: [`MQTTContentValidator.java`](https://github.com/wso2/carbon-device-mgt-plugins/blob/v4.0.55/components/extensions/cdmf-transport-adapters/input/org.wso2.carbon.device.mgt.input.adapter.extension/src/main/java/org/wso2/carbon/device/mgt/input/adapter/extension/validator/MQTTContentValidator.java)

        `public class MQTTContentValidator implements ContentValidator {}`

    2.  Customize the content validator to match the device ID and verify the user and the device.  
        Example: [`MQTTContentValidator.java`](https://github.com/wso2/carbon-device-mgt-plugins/blob/v4.0.55/components/extensions/cdmf-transport-adapters/input/org.wso2.carbon.device.mgt.input.adapter.extension/src/main/java/org/wso2/carbon/device/mgt/input/adapter/extension/validator/MQTTContentValidator.java)

        
        public class MQTTContentValidator implements ContentValidator {
         private static final String JSON_ARRAY_START_CHAR = "[";
         private static final Log log = LogFactory.getLog(MQTTContentValidator.class);
         @Override
         public ContentInfo validate(Object msgPayload, Map < String, String > contentValidationParams,
          Map < String, String > dynamicParams) {
          String topic = dynamicParams.get(MQTTEventAdapterConstants.TOPIC);
          String topics[] = topic.split("/");
          String deviceIdJsonPath = contentValidationParams.get(MQTTEventAdapterConstants.DEVICE_ID_JSON_PATH);
          String deviceIdInTopicHierarchyLevel = contentValidationParams.get(
           MQTTEventAdapterConstants.DEVICE_ID_TOPIC_HIERARCHY_INDEX);
          int deviceIdInTopicHierarchyLevelIndex = 0;
          if (deviceIdInTopicHierarchyLevel != null && !deviceIdInTopicHierarchyLevel.isEmpty()) {
           deviceIdInTopicHierarchyLevelIndex = Integer.parseInt(deviceIdInTopicHierarchyLevel);
          }
          String deviceIdFromTopic = topics[deviceIdInTopicHierarchyLevelIndex];
          boolean status;
          String message = (String) msgPayload;
          if (message.startsWith(JSON_ARRAY_START_CHAR)) {
           status = processMultipleEvents(message, deviceIdFromTopic, deviceIdJsonPath);
          } else {
           status = processSingleEvent(message, deviceIdFromTopic, deviceIdJsonPath);
          }
          return new ContentInfo(status, msgPayload);
         }
        

3.  Configuring the Analytic Receiver by adding the following configurations.

    

    Why configure the receiver?

    

    Once you have configured Entgra IoT Server to transform and validate content, you need to direct the receiver that receives these data to the correct classpaths so as to transform and validate the content accordingly. This needs to be configured in the respective `<DEVICE_TYPE>-receiver-<TENANT-DOMAIN>.xml`.  
    Example: [`raspberrypi_receiver-carbon.super.xml`](https://github.com/wso2/carbon-device-mgt-plugins/blob//v4.0.55/features/device-types-feature/raspberrypi-plugin-feature/org.wso2.carbon.device.mgt.iot.raspberrypi.analytics.feature/src/main/resources/receiver/raspberrypi_receiver-carbon.super.xml) 


    *   **Super Tenant configurations** 

        1.  Add the classpath to where you configured the content transformer.

            If you are using the default method provided by Entgra IoT Server, you need to define the value as `default`. In the default method, the data received will be sent directly to WSO2 DAS without any transformation.

            Example: 

            `<property name="contentTransformer">default</property>`

        2.  Add the classpath to where you configured the content validation.

            If you are using the default method provided by Entgra IoT Server, you need to define the value as `default`. In the default method, the data received will be sent directly to WSO2 DAS without any validation.

            Example:

            `<property name="contentValidator">org.wso2.carbon.device.mgt.iot.input.adapter.mqtt.util.MQTTContentValidator</property>`

    *   **Tenant configurations** Want to configure the analytics receiver for your tenant without using the super tenant configurations? Follow the steps given below:

        1.  Open the respective `<DEVICE_TYPE>-receiver.xml` file that is in the `carbon-device-mgt-plugins/features/device-types-feature/<DEVICE_TYPE>-plugin-feature/org.wso2.carbon.device.mgt.iot.<DEVICETYPE>.backend.feature/src/main/resources/receiver` directory.  
            Example: [`raspberrypi-receiver.xml`](https://github.com/wso2/carbon-device-mgt-plugins/blob/v4.0.55/features/device-types-feature/raspberrypi-plugin-feature/org.wso2.carbon.device.mgt.iot.raspberrypi.backend.feature/src/main/resources/receiver/raspberrypi_receiver.xml)
        2.  Add the classpath to where you configured the content transformer.

            If you are using the default method provided by Entgra IoT Server, you need to define the value as `default`. In the default method, the data received will be sent directly to WSO2 DAS without any transformation.

            Example: 

            `<property name="contentTransformer">default</property>`

        3.  Add the classpath to where you configured the content validation.

            If you are using the default method provided by Entgra IoT Server, you need to define the value as `default`. In the default method, the data received will be sent directly to WSO2 DAS without any validation.

            Example:

            `<property name="contentValidator">org.wso2.carbon.device.mgt.iot.input.adapter.mqtt.util.MQTTContentValidator</property>`

        4.  Restart the Entgra IoT Server's broker, core, and analytics profiles.

            
            cd <IOTS_HOME>/bin

            -----Start the broker profile------
            ./broker.sh or broker.bat

            -----Start the core profile---------
            ./iot-server.sh or iot-server.bat

            ----Start the analytics profile-----
            ./analytics.sh or analytics.bat
            

        5.  Sign in to the device management console using your tenant credentials.  
            If you are not sure of how to create a tenant, see [adding a new tenant](https://entgra-documentation.gitlab.io/v3.7.0/docs/product-administration/tenant-management.html#adding-a-new-tenant).
        6.  Click the menu icon **> CONFIGURATION MANAGEMENT > PLATFORM CONFIGURATIONS**.
        7.  Click on your device specific configuration (e.g., Raspberry Pi Configuration) and click **Deploy Analytics Artifacts**.

            Navigate to the analytics console: `https://<IOTS_HOST>:<IOTS_PORT>/carbon`, click** Receivers** that is on the **Main** tab, and you will see that a new stream has for raspberrypi other than the receiver stream for the raspberrypi_receiver-carbon.super.

            Example:  
            ![image](352822172.png)

  
        **Optionally**, if you want to configure the Analytic Receiver after deploying the artifacts, follow the steps given below:

        1.  Navigate to the analytics console: `https://<IOTS_HOST>:<IOTS_PORT>/carbon`

            The default IoT Server host is `localhost` and the default analytics port on IoT Server is `9445`.

        2.  Click** Receivers** that is on the **Main** tab, and click on the event publisher your want to configure.
        3.  Click Edit and edit the details of the receiver.  
            Example:

            <table>
              <colgroup>
                <col style="width: 175.0px;">
                <col style="width: 1029.0px;">
              </colgroup>
              <tbody>
                <tr>
                  <th>Property</th>
                  <th>Description</th>
                </tr>
                <tr>
                  <th><code>Event Receiver Name</code></th>
                  <td>
                    
                      <p>This is the unique name of your event receiver. The name is similar to what you have defined in your <code>&lt;DEVICE_TYPE&gt;-receiver-&lt;TENANT-DOMAIN&gt;.xml</code> file.<br>Example: <code><a href="https://github.com/wso2/carbon-device-mgt-plugins/blob//v4.0.55/features/device-types-feature/raspberrypi-plugin-feature/org.wso2.carbon.device.mgt.iot.raspberrypi.analytics.feature/src/main/resources/receiver/raspberrypi_receiver-carbon.super.xml" class="external-link" rel="nofollow">raspberrypi_receiver-carbon.super.xml</a></code></p>
          
                  </td>
                </tr>
                <tr>
                  <th><code>Input-Event Adapter Type</code></th>
                  <td>
                    
                      <p>An input event adapter (transport) configuration via which the event receiver receives events.</p>
                      
                        
                          <p>You can define the content transformer and validator properties only if you select <code>oauth-mqtt</code> or <code>XMPP</code>.</p>
           
                  </td>
                </tr>
                <tr>
                  <th><code>Topic</code></th>
                  <td>
                    
                      <p>The name of the topic that the receiver needs to subscribe to.<br>This is similar to what's defined in the <code><a href="https://github.com/wso2/carbon-device-mgt-plugins/blob//v4.0.55/features/device-types-feature/raspberrypi-plugin-feature/org.wso2.carbon.device.mgt.iot.raspberrypi.analytics.feature/src/main/resources/receiver/raspberrypi_receiver-carbon.super.xml" class="external-link" rel="nofollow">raspberrypi_receiver-carbon.super.xml</a></code> file.</p>
              
                    
                  </td>
                </tr>
                <tr>
                  <th><code>contentValidation</code></th>
                  <td>
                    
                      <p><span style="color: rgb(17,17,17);">Type of the content Validation or 'default' to set default type, required to implement (if required)</span></p>
                    
                  </td>
                </tr>
                <tr>
                  <th><code><span style="color: rgb(17,17,17);">contentTransformer</span></code></th>
                  <td><span style="color: rgb(17,17,17);">Type of the content transformer or 'default' to set default type, required to implement (if required)</span></td>
                </tr>
              </tbody>
            </table>

        4.  Click **Update**.

## Adding a Push Notification Provider
By default Entgra IoT Server has implemented push notification providers for MQTT, XMPP, [FCM](https://entgra-documentation.gitlab.io/v3.7.0/docs/Working-with-Android-Devices/Working-with-Android-Devices.html#AndroidNotificationMethods-FCM), and [APNS](https://entgra.atlassian.net/wiki/spaces/IoTS370/pages/352824340/iOS+Notification+Method#iOSNotificationMethod-APNS). This document will guide you on how to create a new push notification provider for your device type.


Follow the steps given below:

1.  Implement the [`PushNotificationProvider`](https://github.com/wso2/carbon-device-mgt/blob/master/components/device-mgt-extensions/org.wso2.carbon.device.mgt.extensions.push.notification.provider.fcm/src/main/java/org/wso2/carbon/device/mgt/extensions/push/notification/provider/fcm/FCMBasedPushNotificationProvider.java)interface by importing `org.wso2.carbon.device.mgt.common.push.notification.PushNotificationProvider`.

    Make sure to export the dependency given below in your `pom.xml` file.

    
    <dependency>
       <groupId>org.wso2.carbon.devicemgt</groupId>
       <artifactId>org.wso2.carbon.device.mgt.common</artifactId>
    </dependency>
    

    For example, see the sample [implementation done for FCM notification provider](https://github.com/wso2/carbon-device-mgt/blob/master/components/device-mgt-extensions/org.wso2.carbon.device.mgt.extensions.push.notification.provider.fcm/src/main/java/org/wso2/carbon/device/mgt/extensions/push/notification/provider/fcm/FCMBasedPushNotificationProvider.java).

    
    package org.wso2.carbon.device.mgt.extensions.push.notification.provider.fcm;
    import org.wso2.carbon.device.mgt.common.push.notification.NotificationStrategy;
    import org.wso2.carbon.device.mgt.common.push.notification.PushNotificationConfig;
    import org.wso2.carbon.device.mgt.common.push.notification.PushNotificationProvider;
    public class FCMBasedPushNotificationProvider implements PushNotificationProvider {
     private static final String PS_PROVIDER_FCM = "FCM";
     @Override
     public String getType() {
      return PS_PROVIDER_FCM;
     }
     @Override
     public NotificationStrategy getNotificationStrategy(PushNotificationConfig config) {
      return new FCMNotificationStrategy(config);
     }
    }
    

2.  Configure the <IoT_HOME>/conf/cdm-config.xml file by adding the reference to your notification provider, under the `PushNotificationProviders` property.

    For example, see the configuration done for the FCM notification provider.

    
    <PushNotificationProviders>
       <Provider>org.wso2.carbon.device.mgt.extensions.push.notification.provider.fcm.FCMBasedPushNotificationProvider</Provider>
       <!--<Provider>org.wso2.carbon.device.mgt.mobile.impl.ios.apns.APNSBasedPushNotificationProvider</Provider>-->
       <Provider>org.wso2.carbon.device.mgt.extensions.push.notification.provider.mqtt.MQTTBasedPushNotificationProvider</Provider>
       <Provider>org.wso2.carbon.device.mgt.extensions.push.notification.provider.xmpp.XMPPBasedPushNotificationProvider</Provider>
    </PushNotificationProviders>
    

3.  Configure your device type XML file to call the push notification provider when the server needs to communicate with the device.   
    For example, [see how it was implemented for the notification provider of RaspberryPi](https://github.com/wso2/carbon-device-mgt-plugins/blob/v4.0.55/features/device-types-feature/raspberrypi-plugin-feature/org.wso2.carbon.device.mgt.iot.raspberrypi.ui.feature/src/main/resources/devicetypes/raspberrypi.xml#L37-L39). 

    
    <PushNotificationProvider type="MQTT">
       <FileBasedProperties>true</FileBasedProperties>
    </PushNotificationProvider>
    

# Writing UI Extensions

In Entgra IoTS each device type has it's own UI that lists the device specific details. Therefore, when writing your own device type you need to write the UI units mentioned below for the device specific details to be listed in the Entgra IoTS Jaggery application pages.

#### Device type view Jaggery application page

The device type UI is written using the  `cdmf.unit.device.type.<DEVICE_TYPE_NAME>.type-view` unit. This unit is fetched when you click on a specific device type from the device-type-listings page.

For more information on the sample implementation of this unit for the Virtual Fire-alarm, see[ here](https://github.com/wso2/carbon-device-mgt-plugins/tree/v4.0.55/components/device-types/virtual-fire-alarm-plugin/org.wso2.carbon.device.mgt.iot.virtualfirealarm.ui/src/main/resources/jaggeryapps/devicemgt/app/units/cdmf.unit.device.type.virtual_firealarm.type-view).


The main `.hbs` file of this unit includes all the information of a specific device type such as:

*   Explain the device type and its functionality. 

*   The steps to register an instance of the device.

*   Links to download a device-agent (if available), other supporting documentation and APIs

![image](2.png)

#### Device details Jaggery application page

The device details UI is written using the `cdmf.unit.device.type.<DEVICE_TYPE_NAME>.device-view` unit. This unit is fetched when you click on a specific instance of a device type that is already created.

*   The main `.``hbs` file of this unit includes all the information related to a specific instance of a device type. The author can retrieve and initialize the `deviceType` and `deviceId`, which are specific to the device instance  from the `request`/`uri` parameters inside the javascript file. Therefore, if any API call needs to be made, you can include the `deviceType` and `deviceId` variables inside the `.``hbs` file.  
    Further to generate the real-time data gathered from the device, you need to write the [Real-time data Jaggery application unit](about:blank#WritingUIExtensions-Real-timedataJaggeryapplicationpage).
*   Include the `device-operations` unit in a `zone` for the available operations of the device to be listed. The listing happens according to the `@Operation` annotation used in the device-type API.

For more information on the sample implementation of this unit for the Virtual Fire-alarm, see[ here](https://github.com/wso2/carbon-device-mgt-plugins/tree/v4.0.55/components/device-types/virtual-fire-alarm-plugin/org.wso2.carbon.device.mgt.iot.virtualfirealarm.ui/src/main/resources/jaggeryapps/devicemgt/app/units/cdmf.unit.device.type.virtual_firealarm.device-view).

![image](352822209.png)

#### Real-time data Jaggery application page

The policy wizard UI is written using the `cdmf.unit.device.type.<DEVICE_TYPE_NAME>.realtime.analytics-view` unit. This unit is fetched when you click on a specific instance of a device type that is already created. 

You need to configure the graphs to define what axis represents the data gathered from the device sensors.

For more information on the sample implementation of this unit for the Virtual Fire-alarm, see [here](https://github.com/wso2/carbon-device-mgt-plugins/tree/v4.0.55/components/device-types/virtual-fire-alarm-plugin/org.wso2.carbon.device.mgt.iot.virtualfirealarm.ui/src/main/resources/jaggeryapps/devicemgt/app/units/cdmf.unit.device.type.virtual_firealarm.realtime.analytics-view).

#### Historical data Jaggery application page

The policy wizard UI is written using the `cdmf.unit.device.type.<DEVICE_TYPE_NAME>.analytics-view` unit. This unit is fetched when you click **View Device Analytics** that is under the real time graph of the device page. You need to configure the graphs to define what axis represents the data gathered from the device sensors.

For more information on the sample implementation of this unit for the Virtual Fire-alarm, see [here](https://github.com/wso2/carbon-device-mgt-plugins/tree/v4.0.55/components/device-types/virtual-fire-alarm-plugin/org.wso2.carbon.device.mgt.iot.virtualfirealarm.ui/src/main/resources/jaggeryapps/devicemgt/app/units/cdmf.unit.device.type.virtual_firealarm.analytics-view).

#### Policy wizard Jaggery application page

The policy wizard UI is written using the `cdmf.unit.device.type.<DEVICE_TYPE_NAME>.policy-wizard` unit. The unit is fetched when you click **ADD**** POLICY** in the Policy Management page. This unit can be configured to inherit the parent policy wizard unit (`iot.unit.policy.wizard`) of the `devicemgt` Jaggery Application. 

*   For more information on the sample implementation of this unit for Android, see [here](https://github.com/wso2/carbon-device-mgt-plugins/tree/v4.0.55/components/mobile-plugins/android-plugin/org.wso2.carbon.device.mgt.mobile.android.ui/src/main/resources/jaggeryapps/devicemgt/app/units/cdmf.unit.device.type.android.policy-wizard).
*   For more information on how to add a policy, see [Adding a Policy](about:blank#).

#### Adding a policy Jaggery application page

Adding a policy UI is written using the `cdmf.unit.device.type.<DEVICE_TYPE_NAME>.policy-view`  unit. The unit is fetched when you click view on a policy that's already created. This unit can be configured to inherit the parent policy view unit ( `iot.unit.policy.view` ) of the  `devicemgt`  jaggery application.

For more information on the sample implementation of this unit for Android, see [here](https://github.com/wso2/carbon-device-mgt-plugins/tree/v4.0.55/components/mobile-plugins/android-plugin/org.wso2.carbon.device.mgt.mobile.android.ui/src/main/resources/jaggeryapps/devicemgt/app/units/cdmf.unit.device.type.android.policy-view).

#### Editing a policy Jaggery application page

Editing a policy UI is written using the `cdmf.unit.device.type.<DEVICE_TYPE_NAME>.policy-edit` unit. This unit is fetched when you click **edit** on a policy that's already created. This unit can be configured to inherit the parent policy-edit unit (`iot.unit.policy.edit`) of the `devicemgt` Jaggery Application.  

For more information on the sample implementation of this unit for Android, see [here](https://github.com/wso2/carbon-device-mgt-plugins/tree/v4.0.55/components/mobile-plugins/android-plugin/org.wso2.carbon.device.mgt.mobile.android.ui/src/main/resources/jaggeryapps/devicemgt/app/units/cdmf.unit.device.type.android.policy-edit).

# Writing Analytics

Entgra IoT Server uses WSO2 Data Analytics Server ( WSO2 DAS) to write batch analytics and process historical sensor data. Your device will have one or more sensors to gather data. For example, the IRS+ Drone has sensors to identify its current location, velocity, and battery charged percentage.  

Before you begin, you need to have an understanding of the IoTS analytics framework. For more information, see **[Entgra IoT Server Analytics](https://entgra-documentation.gitlab.io/v3.7.0/docs/Entgra-IoT-Server-Analytics/Entgra-IoT-Server-Analytics.html)**. Further, let's get an understanding of the DAS artifacts that each sensor in Entgra IoTS needs to have.


*   **Event receivers**  
    WSO2 DAS receives data published by agents or sensors through Event Receivers. Each event receiver is associated with an event stream, which you then persist and/or process using event executor. There are many transport types supported as entry protocols for Event Receivers in WSO2 DAS. For more information on Event Receivers, see the WSO2 DAS [Configuring Event Receivers](https://docs.wso2.com/display/DAS310/Configuring+Event+Receivers).  

*   ****Event streams****  
    The data received from the receiver are then converted to event streams.  

*   **Event Store** Event Store is used to store events that are published directly to the DAS. You need to have the event store unit if you want to analyze the historical data received by the device sensors.  

*   **Spark scripts**  
    Main analytics engine of WSO2 DAS is based on [Apache Spark](http://spark.apache.org/). This is used to perform batch analytics operations on the data stored in Event Stores using analytics scripts written in Spark SQL  

*   **Execution plan** WSO2 DAS uses a real-time event processing engine which is based on Siddhi. For more information on real-time analytics using Siddhi, see the WSO2 DAS documentation on [Realtime Analytics Using Siddhi](https://docs.wso2.com/display/DAS310/Realtime+Analytics+Using+Siddhi).  

*   **Event publisher** Output data either from Spark scripts or Siddhi are published from the DAS using event publishers. Event Processors support various transport protocols.

If you wish to only use real-time analytics for your device type, you only require the Event Receiver, Event Streams, and Event Execution artifacts.  
If you wish to analyze the historical data of the device, you require the Event Store and Spark script artifacts too.

### **Sample implementation**

#### Step 1: Creating the DAS Carbon Application

Let's take a look at how the analytics were written for the Raspberry Pi device type and how it is structured to create a DAS Composite Application (DAS C-App). The DAS Capp defines the artifacts, and ships them to WSO2 DAS as an archive. The sample DAS C-App folder structure is as follows:

![image](352822241.png)

1.  Creating the event stream artifact.
    1.  Create the stream format JSON file using a unique name, such as `org.wso2.iot.raspberrypi_1.0.0`, to stream the temperature data.  
        The stream JSON definition consists of 2 main sections named `metaData` and `payloadData`.

        Make sure to only modify the `payloadData` section to suit your requirement as it contains the data that will be published.

        Example: 

        **Stream JSON format to gather data of the coffee level**

        js
        {
          "name": "org.wso2.iot.raspberrypi",
          "version": "1.0.0",
          "nickName": "raspberrypi",
          "description": "Temperature data received from the raspberrypi",
          "metaData": [{
              "name": "owner",
              "type": "STRING"
            },
            {
              "name": "deviceId",
              "type": "STRING"
            },
            {
              "name": "time",
              "type": "LONG"
            }
          ],
          "payloadData": [{
            "name": "temperature",
            "type": "FLOAT"
          }]
        }
        

    2.  Define the event stream as an artifact by configuring the `artifact.xml` file.

        Why is this step required?

        The `artifact.xml` is added to notify the IoT Server to refer the `org.wso2.iot.raspberrypi_1.0.0.json` file for event streaming.

        Example:

        **Sample artifact.xml**

        
        <artifact name= "raspberrypi_stream" version="1.0.0" type="event/stream" serverRole="DataAnalyticsServer">
           <file>org.wso2.iot.raspberrypi_1.0.0.json</file>
        </artifact>
        

2.  Creating the execution plan artifact to convert the data received and process them.
    1.  Create a STORM based distributed execution plan. For more information, see the WSO2 DAS documentation on [Creating a STORM Based Distributed Execution Plan](https://docs.wso2.com/display/DAS310/Creating+a+STORM+Based+Distributed+Execution+Plan).

        Example:

        
        /* Enter a unique ExecutionPlan */
        @Plan:name('raspberrypi_execution')

        /* Enter a unique description for ExecutionPlan */
        -- @Plan:description('raspberrypi_execution')

        /* define streams/tables and write queries here ... */

        @Import('org.wso2.iot.raspberrypi:1.0.0')
        define stream raspberrypi (meta_owner string, meta_deviceId string, meta_time long, temperature float);

        @Export('org.wso2.iot.devices.temperature:1.0.0')
        define stream temperature (meta_owner string, meta_deviceType string, meta_deviceId string, meta_time long, temperature float);

        from raspberrypi
        select meta_owner, 'raspberrypi' as meta_deviceType, meta_deviceId, meta_time * 1000 as meta_time, temperature
        insert into temperature;
        

    2.  Define the execution/processing unit as an artifact by configuring the `artifact.xml` file.

        Why is this step required?

        The `artifact.xml` is added to notify the IoT Server, to refer the `raspberrypi_execution.siddhiql` file for event processing or execution.

        Example:

        
        <artifact name="raspberrypi_execution" version="1.0.0" type="event/execution-plan" serverRole="DataAnalyticsServer">
           <file>raspberrypi_execution.siddhiql</file>
        </artifact>
    

3.  Creating the event receiver artifact.
    1.  Create an XML file containing the details on binding the stream to the table using receivers.  

        Example:

    
        <eventReceiver name="raspberrypi_receiver" statistics="disable" trace="disable" xmlns="http://wso2.org/carbon/eventreceiver">
            <from eventAdapterType="oauth-mqtt">
                <property name="topic">carbon.super/raspberrypi/+/temperature</property>
                <property name="username">admin</property>
                <property name="contentValidator">org.wso2.carbon.device.mgt.input.adapter.mqtt.util.MQTTContentValidator</property>
                <property name="contentTransformer">default</property>
                <property name="dcrUrl">https://${iot.core.host}:${iot.core.https.port}/dynamic-client-web/register</property>
                <property name="url">tcp://${mqtt.broker.host}:${mqtt.broker.port}</property>
                <property name="cleanSession">true</property>
            </from>
            <mapping customMapping="disable" type="json"/>
            <to streamName="org.wso2.iot.raspberrypi" version="1.0.0"/>
        </eventReceiver>
    

    2.  Define the event receiver as an artifact by configuring the `artifact.xml` file.

        Why is this step required?

        The `artifact.xml` is added to notify the IoT Server, to refer the `raspberrypi_receiver.xml` file for event receiving.

        Example:

    
        <artifact name="raspberrypi_receiver" version="1.0.0" type="event/receiver" serverRole="DataAnalyticsServer">
            <file>raspberrypi_receiver.xml</file>
        </artifact>
    

4.  Create a deployable artifact to create an archive of all the artifacts created above.

    Example:


    <artifacts>
        <artifact name="raspberrypi" version="1.0.0" type="carbon/application">
            <dependency artifact="raspberrypi_stream" version="1.0.0" include="true" serverRole="DataAnalyticsServer"/>
            <dependency artifact="raspberrypi_receiver" version="1.0.0" include="true" serverRole="DataAnalyticsServer"/>
            <dependency artifact="raspberrypi_execution" version="1.0.0" include="true" serverRole="DataAnalyticsServer"/>
        </artifact>
    </artifacts>


5.  Configure the `build.xml` for analytics.

    Why is this step required?

     The `build.xml` file is used to create a normal `.zip` file with a `.car` extension. The zip achieve contains all the artifacts and the `artifacts.xml` at the root level of the zip

    Example: 

    <project name="create-raspberrypi-capps" default="zip" basedir=".">

        <property name="project-name" value="${ant.project.name}"/>
        <property name="target-dir" value="target/carbonapps"/>
        <property name="src-dir" value="src/main/resources/carbonapps"/>

        <property name="Raspberrypi_dir" value="raspberrypi"/>

        <target name="clean">
            <delete dir="${target-dir}" />
        </target>

        <target name="zip" depends="clean">
            <mkdir dir="${target-dir}"/>
            <zip destfile="${target-dir}/${Raspberrypi_dir}.car">
                <zipfileset dir="${src-dir}/${Raspberrypi_dir}"/>
            </zip>
        </target>
    </project>


#### Step 2: Publishing data to WSO2 DAS

Once the DAS C-App is created you need to deploy it. Follow any of the methods given below to publish the historical data retrieved by the sensors, depending on your environment.

*   **Developer/Testing environment**  
    Entgra IoTS is prepackaged with WSO2 DAS features. Therefore, the data gathered from the sensors are published to WSO2 DAS when the DAS C-App is deployed in Entgra IoTS.  
    Follow the steps given below:

    1.  Navigate to the `<IoT_HOME>/broker/bin` directory and start the IoT Server broker profile.

    
        cd <IoT_HOME>/broker/bin
        ./wso2server.sh
    

    2.  Navigate to the `<IoT_HOME>/core/bin` directory and start the IoT Server core profile.

    
        cd <IoT_HOME>/core/bin
        ./wso2server.sh
    

    3.  Navigate to the `<IoT_HOME>/analytics/bin` directory and start the IoT Server analytics profile.

    
        cd <IoT_HOME>/analytics/bin
        ./wso2server.sh
    

*   **Production environment**  
    In a production environment deploying the DAS C-App on Entgra IoTS will not be sufficient. Therefore, you need to configure WSO2 DAS to integrate Entgra IoTS.

#### Step 3: Configuring Entgra IoTS to generate graphs

To generate graphs based on the data gathered from the sensor, you need to configure Entgra IoTS as explained below:

1.  Navigate to the `analytics-data-config.xml` file that is in the `<IoTS_HOME>/conf/analytics` directory.
2.  Configure the fields under the `<AnalyticsDataConfiguration>` tag.

  Each field is described in the `analytics-data-config.xml` file.

    Example: 

    <AnalyticsDataConfiguration>
       <Mode>REMOTE</Mode>
       <URL>http://10.10.10.345:9765</URL>
       <Username>admin_username</Username>
       <Password>admin_password</Password>
       <MaxConnections>200</MaxConnections>
       <MaxConnectionsPerRoute>200</MaxConnectionsPerRoute>
       <SocketConnectionTimeout>60000</SocketConnectionTimeout>
       <ConnectionTimeout>60000</ConnectionTimeout>
    </AnalyticsDataConfiguration>
    
# Writing Device Agents

For the hardware of a specific device type to lucidly communicate with Entgra IoT Server, it is necessary to install/burn a software program into the hardware device. Additionally, the code corresponding to this software program needs to be written in a way that it relates to the device type specific APIs. For more information, see [Writing Device APIs](https://entgra-documentation.gitlab.io/v3.7.0/docs/using-entgra-iot-server/device-manufacturer-guide/extending-entgra-iot-server/#writing-device-apis). The software program is known as the device agent. The agent can be written in any programming language that is supported by the device, such as Java, C, Arduino-C, Python, Lua and more.

The device agent functionality can be explained via the below use cases.

### Use case 1 - Device agent pre-installed on the device by the device manufacturer

A device will have its device type plugin installed on the device and deployed on Entgra IoTS. Let's take a look at how it works.

*   A user purchases a device that has the agent pre-installed. He/she is then considered a device owner.

*   The device owner logs into Entgra IoTS and creates an instance of this device by providing the serial number on the device when enrolling it.

*   Boot the device for it to start communicating via the API's that were written by the device manufacturer.

*   For more information on how it works, try out the [connected cup sample](/doc/en/lb2/Device-Manufacturer-Guide.html).
*   For more information on how the agent was configured for Connected Cup, see the [Connected Cup implementation on GitHub](https://github.com/wso2/product-iots/tree/v3.1.0/modules/distribution/src/core/samples/connectedcup/component/agent/src/main/java/org/coffeeking/agent).

### Use case 2 - Download and install the device agent on the device

Let's take a look at how this works:

*   Implement the downloadable device agent for a device, such as an Arduino UNO board or a Raspberry Pi board.

*   Write the UI, so that a device owner (the individual who owns the device) can download the agent when enrolling or registering the device type.

*   The device owner will download the device agent and install/burn it on to the device type.

*   The device then starts communicating with the Entgra IoTS after it boots up using the API's that were written for the device.

*   For more information, try out the [Android Sense](https://entgra-documentation.gitlab.io/v3.7.0/docs/tutorials/arduino.html) device types.

*   For more information on how the agent was configured for Raspberry Pi, see the [Android Sense implementation on GitHub](https://github.com/wso2/carbon-device-mgt-plugins/tree/v4.0.55/components/device-types/androidsense-plugin/org.wso2.carbon.device.mgt.iot.androidsense.agent).

# Creating a New Device Type via the Maven Archetype

You are able to create a device type plugin in one go using the Maven archetype in Entgra IoT Server. Follow the steps given below:

#### Step 1:  Installing the Maven Archetype

Follow the steps given below to install the Maven archetype on your local system, which is a one-time installation.

1.  Clone the maven archetype from GitHub to a preferred location.

    `git clone -b v1.0.0 --single-branch https://github.com/wso2/carbon-device-mgt-maven-plugin.git`

    Example:

    `git clone -b v1.0.0 --single-branch https://github.com/wso2/carbon-device-mgt-maven-plugin.git`

2.  Navigate to the cloned folder via the console, and install the Maven archetype into your local system.  
    Example: 


    cd <PATH-WHERE-THE-FILE-WAS-CLONED>/carbon-device-mgt-maven-plugin
    mvn clean install


#### Step 2: Creating a new device type

1.  Download Entgra IoT Server, if you have not downloaded it previously.

    

    

    

    1.  [Download Entgra IoT Server](https://storage.googleapis.com/iot-release-public/3.4.0/entgra-iots-3.4.0.zip).

    2.  Copy the downloaded file to a preferred location and unzip it. The unzipped file will be called `<IOTS_HOME>` throughout this documentation.

        

        

        *   The downloaded Entgra IoT Server file is large. Therefore, when unzipping, it might extract halfway through and stop. To avoid this, we recommend that you unzip the file via the terminal.  
            Example:

            `unzip entgra-iots-3.4.0.zip`

        *   The maximum character count supported for a file path in the Windows OS is **260**. If this count is exceeded when extracting the pack into a directory, you will get the `Error 0x80010135: Path too long` error. To overcome this issue use the commands given below:
            *   Create a substring and map the current file path to it.  
                In the example given below, the Entgra IoT Server `.zip` file is located in the `C:\Users\Administrator\Downloads\entgra` directory.

                `C:\Users\Administrator\Downloads\entgra>subst Y: C:\Users\Administrator\Downloads\entgra`

            *   Copy the IoT Server Server zip folder to the new path you created and unzip the file there.  
                Example: Unzip the file in the `Y:` drive.

        

        

    

    

2.  Navigate to the `<IoTS_HOME>/samples` folder.

    `cd <IoTS_HOME>/samples`

3.  Create the new device plugin.

    1.  Run the command given below to start creating the new device type plugin.

        `mvn archetype:generate -DarchetypeCatalog=local`

    2.  You will be prompted to choose the archetype. Select `org.wso2.cdmf.devicetype:cdmf-devicetype-archetype (number may differ)` to create the new device type.

        Example:

    
        [INFO] Scanning for projects...
        [INFO]                                                                         
        [INFO] ------------------------------------------------------------------------
        [INFO] Building Maven Stub Project (No POM) 1
        [INFO] ------------------------------------------------------------------------
        [INFO] 
        [INFO] >>> maven-archetype-plugin:2.4:generate (default-cli) > generate-sources @ standalone-pom >>>
        [INFO] 
        [INFO] <<< maven-archetype-plugin:2.4:generate (default-cli) < generate-sources @ standalone-pom <<<
        [INFO] 
        [INFO] --- maven-archetype-plugin:2.4:generate (default-cli) @ standalone-pom ---
        [INFO] Generating project in Interactive mode
        [INFO] No archetype defined. Using maven-archetype-quickstart (org.apache.maven.archetypes:maven-archetype-quickstart:1.0)
        Choose archetype:
        1: local -> org.wso2.mdm:mdm-android-agent-archetype (Creates a MDM-Android agent project)
        2: local -> org.wso2.iot:mdm-android-agent-archetype (Creates a MDM-Android agent project)
        3: local -> org.wso2.cdmf.devicetype:cdmf-devicetype-archetype (WSO2 CDMF Device Type Archetype)
        4: local -> cdmf.devicetype:cdmf-devicetype-archetype (WSO2 CDMF Device Type Archetype)
        Choose a number or apply filter (format: [groupId:]artifactId, case sensitive contains): : 3
    

        

        The available archetypes are not shown?

        

        

        

        

        The archetypes will not be shown if the wrong local catalog file path is being read. To overcome this issue, follow the steps give below:

        1.  Run the command given below to identify the path that is being read: 

            `mvn archetype:generate -DarchetypeCatalog=local -X`

            

            

            This will run the maven archetype in debug mode and search for the `archetype-catalog.xml` file.

            

            

        2.  Check the output that is given:

            *   If the path is  `~/.m2/repository/archetype-catalog.xml`, you shouldn't be facing an issue.
            *   If the path is different to that given above, you need to move the `archetype-catalog.xml` file from the given location to the repository directory using the command given below:

                `cp ~/.<CURRENT_LOCATION_OF THE_FILE>/archetype-catalog.xml ~/.m2/repository/`

                

                

                The `archetype-catalog.xml` file must be in the `.m2` directory.

                

                

                Example: If the path that is being read is `~/.m2/repository/archetype-catalog.xml`, you need to move the `archetype-catalog.xml` file to the `repository` directory.

                `cp ~/.m2/archetype-catalog.xml ~/.m2/repository/`

        3.  Run the command to start creating the new device type.

            `mvn archetype:generate -DarchetypeCatalog=local`

        

        

        

        

    3.  You will be prompted to provide the values for the properties given below:

    
        Define value for property 'groupId': 
        Define value for property 'artifactId': 
        Define value for property 'version':  3.0-SNAPSHOT:
        Define value for property 'package':  
        Define value for property 'deviceType': 
        Define value for property 'sensorType1':
        Define value for property 'sensorType2':
        Define value for property 'sensorType3':
    

        <table>
          <colgroup>
            <col>
            <col>
            <col>
            <col>
          </colgroup>
          <tbody>
            <tr>
              <th>Property</th>
              <th>Definition</th>
              <th>Example</th>
              <th>Required</th>
            </tr>
            <tr>
              <td><code>groupId</code></td>
              <td>
                
                  <p>The group ID is used to identify a created project uniquely across all projects.</p>
                  
                    
                      <p>This is a mandatory field to generate the folder structure for a project using the Maven Archetype.</p>
                    
                  
                
              </td>
              <td>
                <p><code>org.homeautomation</code></p>
              </td>
              <td>Yes</td>
            </tr>
            <tr>
              <td><code>artifactId</code></td>
              <td>
                
                  <p>The value you define as the <code>artifactId</code> will be given as the folder name for the device type you are creating in the <code>&lt;IoTS_HOME&gt;/samples</code> folder.</p>
                  
                    
                      <p>This is a mandatory field to generate the folder structure for a project using the Maven Archetype.</p>
                    
                  
                
              </td>
              <td><code>currentsensor</code></td>
              <td>Yes</td>
            </tr>
            <tr>
              <td><code>version</code></td>
              <td>
                
                  <p>Provide the version of the new device type plugin you are creating. If you wish to use the default version (3<code>.0-SNAPSHOT</code>) click enter.</p>
                  
                    
                      <p>This is a mandatory field to generate the folder structure for a project using the Maven Archetype.</p>
                    
                  
                
              </td>
              <td><code>3.0-SNAPSHOT</code></td>
              <td>Yes</td>
            </tr>
            <tr>
              <td><code>package</code></td>
              <td>Provide the package name. This is not a mandatory field, therefore, if you do not provide a package name when prompted the value you assigned to the <code>groupId</code> will be used as the default value.</td>
              <td>-</td>
              <td>No</td>
            </tr>
            <tr>
              <td>
                
              </td>
              <td>
                
                  <p>The value you define for the deviceType.</p>
                  
                    
                    
                      
                        
                          <p>Once you create the device plugin the device type will be used in the following places:</p>
                          <p><strong>Analytics scripts<br></strong></p>
                          <ul>
                            <li>
                              <p>The event receiver will have the MQTT publisher topic defined in the <code>&lt;TENANT_DOMAIN&gt;.&lt;DEVICE_TYPE&gt;.&lt;DEVICE_ID&gt;.&lt;OTHER_STRINGS&gt;</code> format.<br>Example: <code><a href="https://github.com/wso2/product-iots/blob/master/modules/samples/connectedcup/component/analytics/src/main/resources/carbonapps/connected_cup/connected_cup_receiver/connected_cup_receiver.xml" class="external-link" rel="nofollow">connected_cup_receiver</a></code></p>
                              
                                
                                  
                                
                              
                            </li>
                            <li>
                              <p>The event stream will have the device type defined as a sample. You are able to configure the stream to suit your requirement.<br>Example: <code><code><a href="https://github.com/wso2/product-iots/blob/master/modules/samples/connectedcup/component/analytics/src/main/resources/carbonapps/connected_cup/connected_cup_stream/org.wso2.iot.connectedcup_1.0.0.json" class="external-link" rel="nofollow">connected_cup_stream</a></code></code></p>
                              
                                
                                  "metaData": [
          {"name": "owner", "type": "STRING"},
          {"name": "deviceId", "type": "STRING"},
          {"name": "DeviceType", "type": "STRING"},
          {"name": "timestamp", "type": "LONG"}
        ]
                                
                              
                            </li>
                          </ul>
                          <ul>
                            <li>
                              <p>The metadata defined in the event stream needs to be defined in the event store. Therefore, if there is an attribute from the deviceType defined in the event stream you need to define it in the event store too.</p>
                            </li>
                          </ul>
                          <p><strong>API</strong></p>
                          <ul>
                            <li>
                              <p>Each API will have permissions defined in the <code>permission.xml</code> file that is in the respective JAXRS directories <code>META-INF</code> file. The permissions for these APIs are only accessible by the device type, therefore, it will include the deviceType you specify.<br>Example: The get device API has the permission path defined as <code>/device-mgt/&lt;DEVICE_TYPE&gt;/user</code>.</p>
                              
                                
                                  &lt;Permission&gt;
           &lt;name&gt;Get device&lt;/name&gt;
           &lt;path&gt;/device-mgt/currentsensor/user&lt;/path&gt;
           &lt;url&gt;/device/*&lt;/url&gt;
           &lt;method&gt;GET&lt;/method&gt;
           &lt;scope&gt;currentsensor_user&lt;/scope&gt;
        &lt;/Permission&gt;
                                
                              
                            </li>
                          </ul>
                          <ul>
                            <li>
                              <p>The device type is defined when creating an API using the <code>@DeviceType</code> annotation.<br>Example:</p>
                              
                                
                                  
                                
                              
                            </li>
                          </ul>
                          <p><strong>UI</strong></p>
                          <ul>
                            <li>The device type UI is written using the&nbsp;<code>cdmf.unit.device.type.&lt;DEVICE_TYPE&gt;.type-view</code>&nbsp;unit.&nbsp;</li>
                            <li>The device details UI is written using the&nbsp;<code>cdmf.unit.device.type.&lt;DEVICE_TYPE&gt;.device-view</code>&nbsp;unit.</li>
                            <li>
                              <p>The <code>config.json</code> file in the <code>cdmf.unit.device.type.&lt;DEVICE_TYPE&gt;.device-view</code> unit includes the device type specific information in the <code>label</code> and <code>downloadAgentUri</code> attributes.<br>Example:</p>
                              
                                
                                  {
          "deviceType": {
            "label": "currentsensor",
            "category": "virtual",
            "downloadAgentUri": "currentsensor/device/download"
          }
        }
                                
                              
                            </li>
                            <li>The device details UI is written using the&nbsp;<code>cdmf.unit.device.type.&lt;DEVICE_TYPE&gt;.realtime.analytics-view</code>&nbsp;unit.&nbsp;</li>
                            <li>The device details UI is written using the&nbsp;<code>cdmf.unit.device.type.&lt;DEVICE_TYPE&gt;.analytics-view</code>&nbsp;unit.</li>
                          </ul>
                        
                      
                    
                  
                
              </td>
              <td><code>currentsensor</code></td>
              <td>Yes</td>
            </tr>
            <tr>
              <td>
                <p><code>sensorType1</code></p>
              </td>
              <td>
                
                  <p>Each device will have specific sensors. Define the sensor for the device type you are creating.</p>
                  
                    
                      <p>Using the Maven Archetype you are only able to define one sensor.</p>
                    
                  
                
              </td>
              <td><code>current</code></td>
              <td>Yes</td>
            </tr>
          </tbody>
        </table>

        Example:

    
        Define value for property 'groupId': : org.homeautomation              
        Define value for property 'artifactId': : currentsensor
        Define value for property 'version':  1.0-SNAPSHOT: : 3.0-SNAPSHOT
        Define value for property 'package':  org.homeautomation: : 
        Define value for property 'deviceType': : currentsensor
        Define value for property 'sensorType1': : currentSensor
        Define value for property 'sensorType2': : waterFlowSensor
        Define value for property 'sensorType3': : voltageSensor
    

    4.  Confirm the property configurations by entering `Yes` or `Y` as the value. If you wish to make any changes to the values you defined, enter `No` as the value to start over again   
        Example:

    
        Confirm properties configuration:
        groupId: org.homeautomation
        artifactId: currentsensor
        version: 3.0.0-SNAPSHOT
        package: org.homeautomation
        deviceType: currentsensor
        sensorType1: currentSensor
        sensorType2: waterFlowSensor
        sensorType3: voltageSensor
        Y: Yes
    

4.  Configure the `device-plugins-deployer.xml` file that is in the `<IoTS_HOME>/samples` directory.

    

    

    The values you assigned to the properties in step 3.c above will be used here.

    

    

    1.  Add the new module under the `<modules>` tag.

    
        <modules>
           <module>{artifactID}</module>
        </modules>

        Example:

        <modules>
           <module>currentsensor</module>
        </modules>
    

    2.  Add the device type feature under the `<featureArtifacts>` tag.

    
        <featureArtifactDef>
           {groupID}:{groupID}.{artifactID}.feature:<version>
        </featureArtifactDef>
    

        Example: 

        <featureArtifactDef>
           org.homeautomation:org.homeautomation.currentsensor.feature:3.0-SNAPSHOT
        </featureArtifactDef>
    

    3.  Add the device type feature group under the `<features>` tag.

        <features>
           <feature>
              <id>{groupID}.{artifactID}.feature.group</id>
              <version>{version}</version>
           </feature>
        </features>

        Example: 

    
        <features>
           <feature>
              <id>org.homeautomation.currentsensor.feature.group</id>
              <version>3.0-SNAPSHOT</version>
           </feature>
        </features>

5.  Navigate to the `<IoTS_HOME>/samples`, and deploy the sample device type you created.

    `mvn clean install -f device-plugins-deployer.xml`

    

    Why is this step required?

    

    This command is required to create the device type feature. When the `samples-deployer.xml` runs, it will create the P2 repository in the device types folder. To deploy the sample device type you created, you need to execute the command given below from the same location as the `samples-deployer.xml`.

    

    

#### Step 3: Try out the new device type

1.  Start Entgra IoT Server.


    cd <IoTS_HOME>/bin
    ./wso2server.sh


2.  Access the device management console. You will see the newly created device on the device page.  
    Example: `https://localhost:9443/devicemgt`

    

    

    For more information, see [accessing the device management console](http://localhost:1313/v3.7.0/docs/using-entgra-iot-server/installation-guide/Running-the-Product/#accessing-the-device-management-console).


    

    

3.  Click on the new device type and download the agent.

    

    Unable to download the agent?

    

    

    

    

    If you run into an error when downloading the agent for the device type you created, follow the steps given below:

    1.  Confirm that the `<DEVICE_TYPE>.war file is created by navigating to <IoT_HOME>/core/repository/deployment/webapps` directory.
    2.  Next, open the `<IoT_HOME>/conf/etc/webapp-publisher-config.xml` file and check if the `<EnabledUpdateApi>` is enabled. If it's not enabled, assign the value as true.  
        Example:

        `<EnabledUpdateApi>true</EnabledUpdateApi>`

    

    

    

    

4.  Copy the downloaded agent to a preferred location and unzip it.
5.  Start the agent of the device type.


    cd <DOWNLOADED_AGENT>
    ./startService.sh


    You will be prompted to enter the time interval that you want for the device to push data to WSO2 DAS, in seconds.  
    Example: Push data every 45 seconds.

    `What's the time-interval (in seconds) between successive Data-Pushes to the WSO2-DAS (ex: '60' indicates 1 minute) > 45`

