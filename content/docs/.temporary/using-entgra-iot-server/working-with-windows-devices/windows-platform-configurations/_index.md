# Windows Platform Configurations

Multiple tenants can use Entgra IoTS while, maintaining tenant based isolation. The Windows configurations enable the tenants to customize the Windows settings based on their own requirements.

Follow the steps given below to configure the Windows platform:

1.  [Sign in to the Entgra IoTS Device Management Console](https://entgra-documentation.gitlab.io/v3.7.0/docs/using-entgra-iot-server/installation-guide/Running-the-Product/#accessing-the-entgra-iot-server-management-console) and click the menu icon.

    ![image](352824798.png)
2.  Click **Configuration Management**.  
    ![image](352824815.png) 
3.  Click **Platform configurations** and click **Windows Configurations**.  
    ![image](352824803.png)
4.  Enter the following:
    *   **Notifier Frequency** -  Specify the time period for the wake-up command to automatically trigger the Entgra IoTS client as the notifier frequency. Ensure to specify the notifier frequency in seconds. 

        

        

        For more information on the notifier type and the notifier frequency, see [Windows Notification Method](/doc/en/lb2/Windows-Notification-Methods.html).

        

        

    *   **End User License Agreement (EULA)** - Provide the license agreement that a user must adhere to, when enrolling an Windows device with Entgra IoTS.

    ![image](352824809.png)

5.  Click **SAVE**.
