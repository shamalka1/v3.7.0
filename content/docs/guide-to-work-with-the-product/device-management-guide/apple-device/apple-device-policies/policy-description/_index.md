---
bookCollapseSection: true
weight: 1
---



# <strong> Apple Device Policy Description </strong>


## <strong> Passcode Policy </strong>

This configuration can be used to set a passcode policy to an iOS Device. Once this configuration profile is installed on a device, corresponding users will not be able to modify these settings on their devices.


<table style="width: 100%;">
    <colgroup>
        <col>
            <col>
    </colgroup>
    <tbody>
        <tr>
            <th><strong>Data keys of Policy</strong></th>
            <th>Description</th>
        </tr>
        <tr>
        </tr>
        <tr>
            <td><strong>Force Passcode</strong></td>
            <td>Determines whether the user is forced to set a PIN. Simply setting this value (and not others) forces the user to enter a passcode, without imposing a length or quality
            </td>
        </tr>
        <tr>
            <td><strong>Allow Simple Value</strong></td>
            <td> Determines whether a simple passcode is allowed. A simple passcode is defined as containing repeated characters, or increasing/decreasing characters (such as 123 or CBA). Setting this value to false is synonymous to setting minComplexChars to ”1”.</td>
        </tr>
        <tr>
            <td><strong>Allow Alphanumeric Value</strong></td>
            <td> Specifies whether the user must also enter alphabetic characters (”abcd”) along with numbers, or if numbers only are sufficient.
            </td>
        </tr>
        <tr>
            <td><strong>Minimum passcode length</strong></td>
            <td>Specifies the minimum overall length of the passcode.
            </td>
        </tr>
        <tr>
            <td><strong>Minimum number of complex characters</strong></td>
            <td>Specifies the minimum number of complex characters that a passcode must contain. A ”complex” character is a character other than a number or a letter, such as &%$#.
                <br>( Should be in between 1-to-730 days or none )</td>
        </tr>
        <tr>
            <td><strong>Passcode history</strong></td>
            <td> When the user changes the passcode, it has to be unique within the last N entries in the history. Minimum value is 1, maximum value is 50.
                <br>( Should be in between 1-to-50 passcodes or none )</td>
        </tr>
        <tr>
            <td><strong>Auto Lock Time in minutes</strong></td>
            <td>Specifies the maximum number of minutes for which the device can be idle (without being unlocked by the user) before it gets locked by the system. Once this limit is reached, the device is locked and the passcode must be entered. The user can edit this setting, but the value cannot exceed the maxInactivity value.
                <br>In macOS, this will be translated to screensaver settings.</td>
        </tr>
        <tr>
            <td><strong>Grace period in minutes for device lock</strong></td>
            <td> The maximum grace period, in minutes, to unlock without entering a passcode. Default is 0, that is no grace period, which requires entering a passcode immediately.
                <br>In macOS, this will be translated to screensaver settings.
            </td>
        </tr>
        <tr>
            <td><strong>Maximum number of failed attempts</strong></td>
            <td>On macOS, this can be set to the number of minutes before the login will be reset after the maxFailedAttempts unsuccessful attempts has been reached. This key requires setting maxFailedAttempts.
                <br>Available in macOS 10.10 and later.</td>
        </tr>
    </tbody>
</table>



## <strong> Wi-Fi Settings </strong>

These configurations can be used to set how devices connect to your wireless network(s), including the necessary authentication information. Once this configuration profile is installed on an iOS device, corresponding users will not be able to modify these settings on their devices.

<table style="width: 100%;">
    <colgroup>
        <col>
            <col>
    </colgroup>
    <tbody>
        <tr>
            <th><strong>Data keys of Policy</strong></th>
            <th>Description</th>
        </tr>
        <tr>
        </tr>
        <tr>
            <td><strong>Service Set Identifier (SSID)</strong></td>
            <td>SSID of the Wi-Fi network to be used.
                <br>In iOS 7.0 and later, this is optional if a DomainName value is provided.
            </td>
        </tr>
        <tr>
            <td><strong>Domain Name</strong></td>
            <td>This field can be provided instead of SSID_STR.
                <br>Available in iOS 7.0 and later and in macOS 10.9 and later.( For Wi-Fi Hotspot 2.0 negotiation )</td>
        </tr>
        <tr>
            <td><strong>Hidden Network</strong></td>
            <td>Besides SSID, the device uses information such as broadcast type and encryption type to differentiate a network. By default (false), it is assumed that all configured networks are open or broadcast. To specify a hidden network, must be true.
            </td>
        </tr>
        <tr>
            <td><strong>Hot Spot</strong></td>
            <td> If true, the network is treated as a hotspot.
                <br>Available in iOS 7.0 and later and in macOS 10.9 and later.
            </td>
        </tr>
        <tr>
            <td><strong>Enable Service Provider Roaming</strong></td>
            <td>If true, allows connection to roaming service providers. Defaults to false.
                <br>Available in iOS 7.0 and later and in macOS 10.9 and later.
        </tr>
        <tr>
            <td><strong>Auto Join</strong></td>
            <td> If true, the network is auto-joined. If false, the user has to tap the network name to join it.
                <br>Available in iOS 5.0 and later and in all versions of macOS.</td>
        </tr>
        <tr>
            <td><strong>Displayed Operator Name </strong></td>
            <td>The operator name to display when connected to this network. Used only with Wi-Fi Hotspot 2.0 access points.
                <br>Available in iOS 7.0 and later and in macOS 10.9 and later.</td>
        </tr>
        <tr>
            <td><strong>Proxy Setup</strong></td>
            <td> Valid values are None, Manual, and Auto.
                <br>Available in iOS 5.0 and later and on all versions of macOS
                <br>If the ProxyType field is set to Manual, the following fields must also be provided
                <ul style="list-style-type:disc;">
                    <li><strong>Proxy Server</strong>: The proxy serverʼs network address.( Server URL or IP Address )</li>
                    <li><strong>Proxy Server Port</strong>: The proxy serverʼs port.</li>
                    <li><strong>Proxy Username</strong>: The username used to authenticate to the proxy server.</li>
                    <li><strong>Proxy Password</strong>: The password used to authenticate to the proxy server.</li>
                    <li><strong>Proxy PAC URL</strong>: The URL of the PAC file that defines the proxy configuration.
                    </li>
                    <li><strong>Allow Proxy PAC FallBack</strong>: . If false, prevents the device from connecting directly to the destination if the PAC file is unreachable. Default is false.
                        <br>Available in iOS 7 and later
                    </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td><strong>Encryption Security Type</strong></td>
            <td>Encryption Security Type field is set to WEP, WPA, or ANY, the following fields may also be provided
                <ul style="list-style-type:disc;">
                    <li><strong>Wi-Fi Password</strong>: Password used for encryption security. Absence of a password does not prevent a network from being added to the list of known networks. The user is eventually prompted to provide the password when connecting to that network.</li>
                    <li><strong>EAP Client Configuration</strong>: In addition to the standard encryption types, it is possible to specify an enterprise profile for a given network via the EAP Client Configuration key. If present, its value is a dictionary with the following keys: The following EAP types are accepted:
                        <ul>
                            <li>13 = TLS</li>
                            <li>17 = LEAP</li>
                            <li>18 = EAP-SIM</li>
                            <li>21 = TTLS</li>
                            <li>23 = EAP-AKA</li>
                            <li>25 = PEAP</li>
                            <li>43 = EAP-FAST</li>
                        </ul>
                        For EAP-TLS authentication without a network payload, install the necessary identity certificates and have your users select EAP-TLS mode in the 802.1X credentials dialog that appears when they connect to the network. For other EAP types, a network payload is necessary and must specify the correct settings for the network.</li>
                    <li><strong>Username</strong>: Unless you enter a user name, this property won't appear in an imported configuration. Users can enter this information by themselves when they authenticate.
                    </li>
                    <li><strong>Password</strong>: If not provided, the user will be prompted during login.</li>
                    <li><strong>One Time Password</strong>: If checked, the user will be prompted for a password each time they connect to the network.
                    </li>
                    <li><strong>TLS Trusted Server Certificate Names</strong>: This is the list of server certificate common names that will be accepted. You can use wildcards to specify the name, such as wpa.*.example.com. If a server presents a certificate that isn't in this list, it won't be trusted. Used alone or in combination with TLSTrustedCertificates, the property allows someone to carefully craft which certificates to trust for the given network, and avoid dynamically trusted certificates.
                    </li>
                    <li><strong>Allow TLS Trust Exceptions</strong>: Allows / disallows a dynamic trust decision by the user. The dynamic trust is the certificate dialogue that appears when a certificate isn't trusted. If this is unchecked, the authentication fails if the certificate isn't already trusted.
                    </li>
                    <li><strong>Require TLS Certificate</strong>: If checked, allows for two-factor authentication for EAP-TTLS, PEAP or EAP-FAST. If unchecked, allows for zero factor authentication for EAP-TLS. By default this is enabled for EAP-TLS and disabled for other EAP types. Available in iOS 7.0 and later.
                    </li>
                    <li><strong>TTLS Inner Authentication Type</strong>: Specifies the inner authentication used by the TTLS module. Possible values are PAP, CHAP, MSCHAP and MSCHAPv2.
                    </li>
                    <li><strong>Outer Identity</strong>: This key is only relevant to TTLS, PEAP, and EAP-FAST. This allows the user to hide his or her identity. The userʼs actual name appears only inside the encrypted tunnel. For example, it could be set to ”anonymous” or ”anon”, or ”anon@mycompany.net”. It can increase security because an attacker canʼt see the authenticating userʼs name in the clear.
                    </li>
                    <li><strong>EAP-Fast Support</strong>: <strong>
                        <ul>
                            <li>Use existing PAC for EAP-FAST
                            </li>
                            <li>Allow PAC Provisioning</li>
                            <li>Allow Anonymous PAC Provisioning
                            </li>
                        </ul>
                        </strong>These keys are hierarchical in nature. : If Use existing PAC for EAP-FAST is false, the other two properties arenʼt consulted. Similarly, if Allow PAC Provisioning is false, Allow Anonymous PAC Provisioning isnʼt consulted.
                        <br>If Use existing PAC for EAP-FAST is false, authentication proceeds much like PEAP or TTLS: the server proves its identity using a certificate each time.If checked, the device will use an existing PAC. Otherwise, the server must present its identity using a certificate.
                        <br>If Allow PAC Provisioning is checked, allows PAC provisioning. This particular attribute must be enabled for EAP-FAST PAC usage to succeed, because there is no other way to provision a PAC.
                        <br>If Allow Anonymous PAC Provisioning is checked, provisions the device anonymously. Note that there are known man-in-the-middle attacks for anonymous provisioning.
                    </li>
                    <li><strong>Number of expected RANDs for EAP-SIM</strong>: Number of expected RANDs for EAPSIM. Valid values are 2 and 3. Defaults to 3.
                    </li>
                    <li><strong>Certificate Payload UUID</strong>: UUID of the certificate payload to use for the identity credential.
                    </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td><strong>Roaming Consortium OIs</strong></td>
            <td>Roaming Consortium Organization Identifiers used for Wi-Fi Hotspot 2.0negotiation. Requires 6 or 10 hexadecimal characters.
                <br>Available in iOS 7.0 and later and in macOS 10.9 and later</td>
        </tr>
        <tr>
            <td><strong>Network Access Identifier ( NAI ) Realm Names</strong></td>
            <td> List of Network Access Identifier Real names used for Wi-Fi Hotspot 2.0 negotiation.
                <br>Available in iOS 7.0 and later and in macOS 10.9 and later.</td>
        </tr>
        <tr>
            <td><strong>Mobile Country Code ( MCC ) / Mobile Network Code ( MNC ) Configuration</strong></td>
            <td> List of Mobile Country Code (MCC)/Mobile Network Code (MNC) pairs used for Wi-Fi Hotspot 2.0 negotiation. Each string must contain exactly six digits.
                <br>Available in iOS 7.0 and later. This feature is not supported in macOS</td>
        </tr>
    </tbody>
</table>



## <strong> Global Proxy Settings </strong>

Configure a global HTTP proxy to direct all HTTP traffic from Supervised iOS 7 and higher devices through a designated proxy server. Once this configuration profile is installed on a device, all the network traffic will be routed through the proxy server

<i>This policy is only applicable for the devices enrolled in supervised mode.</i>


<table style="width: 100%;">
    <colgroup>
        <col>
            <col>
    </colgroup>
    <tbody>
        <tr>
            <th><strong>Data keys of Policy</strong></th>
            <th>Description</th>
        </tr>
        <tr>
        </tr>
        <tr>
            <td><strong>Proxy Configuration Type</strong></td>
            <td>If you choose manual proxy type, you need the proxy server
                address including its port and optionally a username and
                password into the proxy server. If you choose auto proxy type,
                you can enter a proxy autoconfiguration (PAC) URL.
            </td>
        </tr>
        <tr>
            <td><strong>Proxy Host</strong></td>
            <td>The proxy serverʼs network address.(Host name/IP address of the proxy server.)</td>
        </tr>
        <tr>
            <td><strong>Proxy Port</strong></td>
            <td>The proxy serverʼs port</td>
        </tr>
        <tr>
            <td><strong>Username</strong></td>
            <td> The username used to authenticate to the proxy
                server.</td>
        </tr>
        <tr>
            <td><strong>Password</strong></td>
            <td>The password used to authenticate to the proxy server</td>
        </tr>
        <tr>
            <td><strong>Allow Captive Login</strong></td>
            <td>When checked, Allows the device to bypass the proxy server to display the login page for captive networks.</td>
        </tr>
    </tbody>
</table>



## <strong> Email Settings </strong>

These configurations can be used to define settings for connecting to your POP or IMAP 
email accounts. Once this configuration profile is installed on an iOS device, corresponding 
users will not be able to modify these settings on their devices.


<table style="width: 100%;">
    <colgroup>
        <col>
            <col>
    </colgroup>
    <tbody>
        <tr>
            <th><strong>Data keys of Policy</strong></th>
            <th>Description</th>
        </tr>
        <tr>
        </tr>
        <tr>
            <td><strong>Account Description</strong></td>
            <td>A user-visible description of the email account, shown in the Mail and Settings applications.
            </td>
        </tr>
        <tr>
            <td><strong>Account Type</strong></td>
            <td>Defines the protocol to be used for that account.</td>
        </tr>
        <tr>
            <td><strong>Path Prefix</strong></td>
            <td>The path prefix for the IMAP mail server</td>
        </tr>
        <tr>
            <td><strong>Email Account Name</strong></td>
            <td>The full user name for the account. This is the user name in sent messages, etc.
        </tr>
        <tr>
            <td><strong>Email Address</strong></td>
            <td>Designates the full email address for the account. If not present in the payload, the device prompts for this string during profile installation.</td>
        </tr>
        <tr>
            <td><strong>Prevent move</strong></td>
            <td>If true, messages may not be moved out of this email account into another account. Also prevents forwarding or replying from a different account than the message was originated from.
                <br> Availability: Available only in iOS 5.0 and later.</td>
        </tr>
        <tr>
            <td><strong>Prevent App Sheet</strong></td>
            <td>If true, this account is not available for sending mail in any app other than the Apple Mail app.
                <br> Availability: Available only in iOS 5.0 and later</td>
        </tr>
        <tr>
            <td><strong>Enable S/MIME</strong></td>
            <td> If true, this account supports S/MIME. As of iOS 10.0, this key is ignored.
                <br> Availability: Available only in iOS 5.0 through iOS 9.3.3.
            </td>
        </tr>
        <tr>
            <td><strong>S/MIME Signing Certificate UUID</strong></td>
            <td>The PayloadUUID of the identity certificate used to sign messages sent from this account.
                <br> Availability: Available only in iOS 5.0 and later</td>
        </tr>
        <tr>
            <td><strong>Enable Per-message Signing and Encryption Switch</strong></td>
            <td>If set to true, display the per-message encryption switch in the Mail Compose UI.
                <br> Availability: Available only in iOS 12.0 and later
            </td>
        </tr>
        <tr>
            <td><strong>Allow Recent Address Syncing</strong></td>
            <td>If true, this account is excluded from address Recents syncing. This defaults to false.
                <br> Availability: Available only in iOS 6.0 and later.
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <center><strong>Incomming Mail Settings</strong></center>
            </td>
        </tr>
        <tr>
            <td><strong>Mail Server Hostname</strong></td>
            <td>Designates the incoming mail server host name (or IP address).
            </td>
        </tr>
        <tr>
            <td><strong>Use Secure Socket Layer(SSL)</strong></td>
            <td>Designates whether the incoming mail server uses SSL for authentication.</td>
        </tr>
        <tr>
            <td><strong>Mail Server Port</strong></td>
            <td> Designates the incoming mail server port number. If no port number is specified, the default port for a given protocol is used.</td>
        </tr>
        <tr>
            <td><strong>Authentication Type</strong></td>
            <td>Designates the authentication scheme for incoming mail. Allowed values are EmailAuthPassword, EmailAuthCRAMMD5, EmailAuthNTLM, EmailAuthHTTPMD5, and EmailAuthNone.
            </td>
        </tr>
        <tr>
            <td><strong>Username</strong></td>
            <td>Designates the user name for the email account, usually the same as the email address up to the @ character. If not present in the payload, and the account is set up to require authentication for incoming email, the device will prompt for this string during profile installation.</td>
        </tr>
        <tr>
            <td><strong>Password</strong></td>
            <td> Password for the Incoming Mail Server. Use only with encrypted profiles.</td>
        </tr>
        <tr>
            <td colspan="2">
                <center><strong>Outgoing Mail Settings</center></strong></td>
        </tr>
        <tr>
            <td><strong>Mail Server Hostname</strong></td>
            <td>Designates the outgoing mail server host name (or IP address).
            </td>
        </tr>
        <tr>
            <td><strong>Use Server Socket Layer(SSL)</strong></td>
            <td>Default false. Designates whether the outgoing mail server uses SSL for authentication.
            </td>
        </tr>
        <tr>
            <td><strong>Mail Server Port</strong></td>
            <td> Designates the outgoing mail server port number. If no port number is specified, ports 25, 587 and 465 are used, in this order.</td>
        </tr>
        <tr>
            <td><strong>Authentication Type</strong></td>
            <td>Designates the authentication scheme for outgoing mail. Allowed values are EmailAuthPassword, EmailAuthCRAMMD5, EmailAuthNTLM, EmailAuthHTTPMD5, and EmailAuthNone.</td>
        </tr>
        <tr>
            <td><strong>Username</strong></td>
            <td>Designates the user name for the email account, usually the same as the email address up to the @ character. If not present in the payload, and the account is set up to require authentication for outgoing email, the device prompts for this string during profile installation</td>
        </tr>
        <tr>
            <td><strong>Password</strong></td>
            <td> Password for the Outgoing Mail Server. Use only with encrypted profiles.</td>
        </tr>
    </tbody>
</table>

## <strong> AirPlay Settings </strong>

This configuration can be used to define settings for connecting to AirPlay destinations. 
Once this configuration profile is installed on an iOS device, corresponding users will not be able 
to modify these settings on their devices.


<table style="width: 100%;">
    <colgroup>
        <col>
            <col>
    </colgroup>
    <tbody>
        <tr>
            <th><strong>Data keys of Policy</strong></th>
            <th>Description</th>
        </tr>
        <tr>
            <td colspan="2">
                <center><strong>AirPlay Credentials</strong>
                    <br>(If present, sets passwords for known AirPlay destinations.)</center>
            </td>
        </tr>
        <tr>
            <td><strong>Device Name</strong></td>
            <td>The name of the AirPlay destination (used on iOS).
            </td>
        </tr>
        <tr>
            <td><strong>Password</strong></td>
            <td>The password for the AirPlay destination.</td>
        </tr>
        <tr>
            <td colspan="2">
                <center><strong>AirPlay Destinations Whitelist</strong>
                    <br>(Supervised only. If present, only AirPlay destinations present in this list are available to the device.)</center>
            </td>
        </tr>
        <tr>
            <td><strong>Destination</strong></td>
            <td>The Device ID of the AirPlay destination, in the format xx:xx:xx:xx:xx:xx. This field is not case sensitive.
            </td>
        </tr>
    </tbody>
</table>


## <strong> Manage Domains </strong>

This payload defines web domains that are under an enterprise’s management.


<table style="width: 100%;">
    <colgroup>
        <col>
            <col>
    </colgroup>
    <tbody>
        <tr>
                <th><strong>Data keys of Policy</strong></th>
                <th>Description</th>
        </tr>
        <tr>
            <td colspan="2">
                <center><strong>Unmarked Email Domains</strong>
                    <br> (Any email address that does not have a suffix that matches one of the unmarked email domains specified by the key EmailDomains will be considered out-of-domain and will be highlighted as such in the Mail app.)</center>
            </td>
        </tr>
        <tr>
            <td><strong>Email Domains</strong></td>
            <td>An array of strings. An email address lacking a suffix that matches any of these strings will be considered out-of-domain.
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <center><strong>Managed Safari Web Domains</strong>
                    <br>(Supervised only. If present, only AirPlay destinations present in this list are available to the device.)</center>
            </td>
        </tr>
        <tr>
            <td><strong>Managed Safari Web Domains</strong></td>
            <td>An array of URL strings. URLs matching the patterns listed here will be considered managed.
            </td>
        </tr>
    </tbody>
</table>



## <strong> LDAP Settings </strong>

This configuration can be used to define settings for connecting to LDAP servers. Once this 
configuration profile is installed on an iOS device, corresponding users will not be able to modify 
these settings on their devices.


<table style="width: 100%;">
    <colgroup>
        <col>
            <col>
    </colgroup>
    <tbody>
        <tr>
            <th><strong>Data keys of Policy</strong></th>
            <th>Description</th>
        </tr>
        <tr>
            <th><strong>Data keys of Policy</strong></th>
            <th>Description</th>
        </tr>
        <tr>
        </tr>
        <tr>
            <td><strong>Account Description</strong></td>
            <td>Display name of the account
            </td>
        </tr>
        <tr>
            <td><strong>Account Hostname</strong></td>
            <td>LDAP Host name or IP address</td>
        </tr>
        <tr>
            <td><strong>Use Secure Socket Layer(SSL)</strong></td>
            <td>Having this checked, would enable Secure Socket Layer communication.</td>
        </tr>
        <tr>
            <td><strong>Account Username</strong></td>
            <td>User name for this LDAP account</td>
        </tr>
        <tr>
            <td><strong>Account Password</strong></td>
            <td>Password for this LDAP account</td>
        </tr>
        <tr>
            <td colspan="2">
                <center><strong>Search Settings </strong>
                    <br> Search settings for this LDAP account. Can have many of these for one account. Should have at least one for the account to be useful.</center>
            </td>
        </tr>
        <tr>
            <td><strong>Description</strong></td>
            <td>Description of this search setting</td>
        </tr>
        <tr>
            <td><strong>Search Base</strong></td>
            <td> Conceptually, the path to the node where a search should start. For example: ou=people,o=example corp
            </td>
        </tr>
        <tr>
            <td><strong>Scope</strong></td>
            <td>Defines what recursion to use in the search. Can be one of the following 3 values:
                <ul style="list-style-type:disc;">
                    <li>LDAPSearchSettingScopeBase: Just the immediate node pointed to by SearchBase
                    </li>
                    <li>LDAPSearchSettingScopeOneLevel: The node plus its immediate children .
                    </li>
                    <li>LDAPSearchSettingScopeSubtree: The node plus all children, regardless of depth.</li>
                </ul>
            </td>
        </tr>
    </tbody>
</table>

## <strong> ActiveSync Configurations </strong>

This configuration can be used to provision ActiveSync Configurations for iOS devices.

<table style="width: 100%;">
    <colgroup>
        <col>
            <col>
    </colgroup>
    <tbody>
        <tr>
            <th><strong>Data keys of Policy</strong></th>
            <th>Description</th>
        </tr>
        <tr>
        </tr>
        <tr>
            <td><strong>Email Address</strong></td>
            <td>Specifies the full email address for the account. If not present in the payload, the device prompts for this string during profile installation.
                <br> In macOS, this key is required
            </td>
        </tr>
        <tr>
            <td><strong>Exchange Server Hostname</strong></td>
            <td>Specifies the Exchange server host name (or IP address).
                <br> In macOS 10.11 and later, this key is optional.
            </td>
        </tr>
        <tr>
            <td><strong>Use Secure Socket Layer(SSL)</strong></td>
            <td>Specifies whether the Exchange server uses SSL for authentication.</td>
        </tr>
        <tr>
            <td><strong>Account Username</strong></td>
            <td>This string specifies the user name for this Exchange account.
                <br> Required in macOS or non-interactive installations (like MDM on iOS).</td>
        </tr>
        <tr>
            <td><strong>Account Password</strong></td>
            <td>The password of the account. Use only with encrypted profiles.</td>
        </tr>
        <tr>
            <td><strong>Use OAuth</strong></td>
            <td>Specifies whether the connection should use OAuth for authentication. If enabled, a password should not be specified. This defaults to false.
                <br> Availability: Available only in iOS 12.0 and macOS 10.14 and later.</td>
        </tr>
        <tr>
            <td colspan="2">
                <center><strong>Available in iOS only</strong>
                </center>
            </td>
        </tr>
        <tr>
            <td><strong>ActiveSync Certificate file</strong></td>
            <td>For accounts that allow authentication via certificate, a .p12 identity certificate in NSData blob format
            </td>
        </tr>
        <tr>
            <td><strong>Certificate Name</strong></td>
            <td> Specifies the name or description of the certificate
            </td>
        </tr>
        <tr>
            <td><strong>Certificate Password</strong></td>
            <td>The password necessary for the p12 identity certificate. Used with mandatory encryption of profiles.
            </td>
        </tr>
        <tr>
            <td><strong>Prevent Move</strong></td>
            <td>If set to true, messages may not be moved out of this email account into another account. Also prevents forwarding or replying from a different account than the message was originated from.
                <br> Availability: Available in iOS 5.0 and later.
            </td>
        </tr>
        <tr>
            <td><strong>Prevent App Sheet</strong></td>
            <td>If set to true, this account will not be available for sending mail in any app other than the Apple Mail app.
                <br> Availability: Available in iOS 5.0 and later
            </td>
        </tr>
        <tr>
            <td><strong>Payload Certificate UUID</strong></td>
            <td>UUID of the certificate payload to use for the identity credential. If this field is present, the Certificate field is not used.
                <br> Availability: Available in iOS 5.0 and later
            </td>
        </tr>
        <tr>
            <td><strong>SMIME Enabled</strong></td>
            <td> If true, this account supports S/MIME. As of iOS 10.0, this key is ignored.
                <br> Availability: Available only in iOS 5.0 through 9.3.3.
            </td>
        </tr>
        <tr>
            <td><strong>SMIME Signing Enabled</strong></td>
            <td>If set to true, S/MIME signing is enabled for this account.
                <br> Availability: Available only in iOS 10.3 and later
            </td>
        </tr>
        <tr>
            <td><strong>SMIME Signing Certificate UUID</strong></td>
            <td> The PayloadUUID of the identity certificate used to sign messages sent from this account.
                <br> Availability: Available only in iOS 5.0 and later.
            </td>
        </tr>
        <tr>
            <td><strong>SMIME Encryption Enabled</strong></td>
            <td>If set to true, S/MIME encryption is on by default for this account.
                <br> Availability: Available only in iOS 10.3 and later. As of iOS 12.0, this key is deprecated. It is recommended to use SMIMEEncryptByDefault instead.
            </td>
        </tr>
        <tr>
            <td><strong>SMIME Encryption Certificate UUID</strong></td>
            <td>The PayloadUUID of the identity certificate used to decrypt messages sent to this account. The public certificate is attached to outgoing mail to allow encrypted mail to be sent to this user. When the user sends encrypted mail, the public certificate is used to encrypt the copy of the mail in their Sent mailbox.
                <br> Availability: Available only in iOS 5.0 and later.
            </td>
        </tr>
        <tr>
            <td><strong>SMIME Enable PerMessage Switch</strong></td>
            <td>The password necessary for the p12 identity certificate. Used with mandatory encryption of profiles.
            </td>
        </tr>
        <tr>
            <td><strong>SMIME Signing User Overrideable</strong></td>
            <td>T If set to true, the user can toggle S/MIME signing on or off in Settings.
                <br> Availability: Available only in iOS 12.0 and later.
            </td>
        </tr>
        <tr>
            <td><strong>SMIME Signing Certificate UUID UserOverrideable</strong></td>
            <td>If set to true, the user can select the signing identity.
                <br> Availability: Available only in iOS 12.0 and later.
            </td>
        </tr>
        <tr>
            <td><strong>SMIME Encrypt By Default</strong></td>
            <td> If set to true, S/MIME encryption is enabled by default. If SMIMEEnableEncryptionPerMessageSwitch is false, this default cannot be changed by the user.
                <br> Availability: Available only in iOS 12.0 and later.
            </td>
        </tr>
        <tr>
            <td><strong>SMIME Encrypt By Default User Overrideable</strong></td>
            <td> If set to true, the user can toggle the encryption by default setting.
                <br> Availability: Available only in iOS 12.0 and later.
            </td>
        </tr>
        <tr>
            <td><strong>SMIME Encryption Certificate UUID User Overrideable</strong></td>
            <td>If set to true, the user can select the S/MIME encryption identity and encryption is enabled.
                <br> Availability: Available only in iOS 12.0 and later.
            </td>
        </tr>
        <tr>
            <td><strong>SMIME Enable Encryption Per-Message Switch</strong></td>
            <td>If set to true, displays the per-message encryption switch in the Mail Compose UI.
                <br> Availability: Available only in iOS 12.0 and later
            </td>
        </tr>
        <tr>
            <td><strong>Allow Mail drop</strong></td>
            <td>If true, this account is allowed to use Mail Drop. The default is false.
                <br> Availability: Available only in macOS 10.12 and later.
            </td>
        </tr>
        <tr>
            <td><strong>Disable Mail Recents Syncing</strong></td>
            <td>If true, this account is excluded from address Recents syncing.
                <br> Availability: Available only in iOS 6.0 and later.
            </td>
        </tr>
        <tr>
            <td><strong>Mail Number Of PastDays To Sync</strong></td>
            <td>The number of days since synchronization.
            </td>
        </tr>
        <tr>
            <td><strong>Bundle ID of Default Application Handling Audio Calls</strong></td>
            <td>The communication service handler rules for this account. The CommunicationServiceRules dictionary currently contains only a DefaultServiceHandlers key; its value is a dictionary which contains an AudioCall key whose value is a string containing the bundle identifier for the default application that handles audio calls made to contacts from this account.
            </td>
        </tr>
    </tbody>
</table>

## <strong> Calendar </strong>

This configuration can be used to define settings for connecting to CalDAV servers. Once this configuration profile is installed on an iOS device, corresponding users will not be able to modify these settings on their devices.

<table style="width: 100%;">
    <colgroup>
        <col>
            <col>
    </colgroup>
    <tbody>
        <tr>
            <th><strong>Data keys of Policy</strong></th>
            <th>Description</th>
        </tr>
        <tr>
        </tr>
        <tr>
            <td><strong>Account Description</strong></td>
            <td>Display name of the account. Eg: Company CalDAV Account
            </td>
        </tr>
        <tr>
            <td><strong>Account Hostname</strong></td>
            <td>CalDAV Host name or IP address</td>
        </tr>
        <tr>
            <td><strong>Use Secure Socket Layer(SSL)</strong></td>
            <td>Having this checked, would enable Secure Socket Layer communication with CalDAV server.
            </td>
        </tr>
        <tr>
            <td><strong>Account Port</strong></td>
            <td>CalDAV account Host Port number</td>
        </tr>
        <tr>
            <td><strong>Principal URL</strong></td>
            <td>Principal URL for the CalDAV account</td>
        </tr>
        <tr>
            <td><strong>Account Username</strong></td>
            <td>CalDAV account user name</td>
        </tr>
        <tr>
            <td><strong>Account Password</strong></td>
            <td>CalDAV account password
            </td>
        </tr>
    </tbody>
</table>


## <strong> Calendar Subscription </strong>

This configuration can be used to define settings for calendar subscriptions. Once this configuration profile is installed on an iOS device, corresponding users will not be able to modify these settings on their devices.


<table style="width: 100%;">
    <colgroup>
        <col>
            <col>
    </colgroup>
    <tbody>
        <tr>
            <th><strong>Data keys of Policy</strong></th>
            <th>Description</th>
        </tr>
        <tr>
        </tr>
        <tr>
            <td><strong>Description</strong></td>
            <td>Description of the account.
            </td>
        </tr>
        <tr>
            <td><strong>Account Hostname URL</strong></td>
            <td>The server address.</td>
        </tr>
        <tr>
            <td><strong>Use Secure Socket Layer(SSL)</strong></td>
            <td>Having this checked, would enable Secure Socket Layer communication.
            </td>
        </tr>
        <tr>
            <td><strong>Username</strong></td>
            <td>The userʼs login name.</td>
        </tr>
        <tr>
            <td><strong>Password</strong></td>
            <td>The userʼs password.</td>
        </tr>
    </tbody>
</table>


## <strong> Cellular Network Settings </strong>
 
These configurations can be used to specify Cellular Network Settings on an iOS device. Cellular settings cannot be installed if an APN setting is already installed and upon successful installation, corresponding users will not be able to modify these settings on their devices.

<i> (This feature is supported only on iOS 7.0 and later.) </i>

<table style="width: 100%;">
    <colgroup>
        <col>
            <col>
    </colgroup>
    <tbody>
        <tr>
            <th><strong>Data keys of Policy</strong></th>
            <th>Description</th>
        </tr>
        <tr>
            <td><strong>Cellular Configuration Name </strong></td>
            <td>The Access Point Name.
            </td>
        </tr>
        <tr>
            <td><strong>Authentication Type</strong></td>
            <td>Must contain either CHAP or PAP. Defaults to PAP.</td>
        </tr>
        <tr>
            <td><strong>Username</strong></td>
            <td> A user name used for authentication.
            </td>
        </tr>
        <tr>
            <td><strong>Password</strong></td>
            <td>A password used for authentication.</td>
        </tr>
        <tr>
            <td colspan="2">
                <center><strong>APN Configurations </strong></center>
            </td>
        </tr>
        <tr>
            <td><strong>APN</strong></td>
            <td>The Access Point Name.
            </td>
        </tr>
        <tr>
            <td><strong>Auth.Type</strong></td>
            <td>Must contain either CHAP or PAP. Defaults to PAP.</td>
        </tr>
        <tr>
            <td><strong>Username</strong></td>
            <td>A user name used for authentication.
            </td>
        </tr>
        <tr>
            <td><strong>Password</strong></td>
            <td>A password used for authentication.</td>
        </tr>
        <tr>
            <td><strong>Proxy</strong></td>
            <td>The proxy serverʼs network address.</td>
        </tr>
        <tr>
            <td><strong>Port</strong></td>
            <td>The proxy serverʼs port.</td>
        </tr>
    </tbody>
</table>

## <strong> Network Usage Rules </strong>
 
Network Usage Rules allow enterprises to specify how managed apps use networks, such as cellular data networks.

<i>These rules only apply to managed apps.</i>


<table style="width: 100%;">
    <colgroup>
        <col>
            <col>
    </colgroup>
    <tbody>
        <tr>
            <th><strong>Data keys of Policy</strong></th>
            <th>Description</th>
        </tr>
        <tr>
            <td><strong>Allow cellular data when roaming</strong>
                <br><i>(Common to all rule 
            configuration types)</i></td>
            <td>If set to false, matching managed apps will not be allowed to use cellular data when roaming.</td>
        </tr>
        <tr>
            <td><strong>Allow Cellular Data</strong>
                <br><i>(Common to all rule 
                                                                configuration types)</i></td>
            <td> If set to false, matching managed apps will not be allowed to use cellular data at any time.
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <center><strong>Applly to specified managed apps</strong>
                    <br>(Set network usage rules to specific applications)
                </center>
            </td>
        </tr>
        <tr>
            <td><strong>Application Identifier Match</strong></td>
            <td>A list of managed app identifiers, as strings, that must follow the associated rules. If this key is missing, the rules will apply to all managed apps on the device
                <br>Each string in the Application Identifier Match may either be an exact app identifier match,
                <br><i>[e.g . com.mycompany.myapp]</i>
                <br>or it may specify a prefix match for the Bundle ID by using the * wildcard character. The wildcard character, if used, must appear after a period character (.), and may only appear once, at the end of the string
                <br><i>[e.g. com .mycompany .*.]</i></td>
        </tr>
    </tbody>
</table>


## <strong> VPN(Virtual Private Network) Settings </strong>

This configurations can be used to configure VPN settings on an iOS device. Once this configuration 
profile is installed on a device, corresponding users will not be able to modify these settings on their devices.

<i>Please note that * sign represents required fields of data.</i>


<table style="width: 100%;">
    <colgroup>
        <col>
            <col>
    </colgroup>
    <tbody>
        <tr>
            <th><strong>Data keys of Policy</strong></th>
            <th>Description</th>
        </tr>
        <tr>
            <td><strong>Connection Name</strong></td>
            <td>Description of the VPN connection displayed on the device.
            </td>
        </tr>
        <tr>
            <td><strong>Override Primary</strong></td>
            <td>Specifies whether to send all traffic through the VPN interface. If true, all network traffic is sent over VPN.</td>
        </tr>
        <tr>
            <td><strong>On-demand Enabled</strong></td>
            <td>Check if the VPN connection should be brought up on demand, else leave un-checked.
            </td>
        </tr>
        <tr>
            <td><strong>VPN Type</strong></td>
            <td>Determines the settings available in the payload for this type of VPN connection. It can have one of the following values:
                <ul style="list-style-type:disc;">
                    <li>L2TP</li>
                    <li>PPTP</li>
                    <li>IPSec (Cisco)</li>
                    <li> IKEv2 (see IKEv2 Dictionary Keys)</li>
                    <li>AlwaysOn (see AlwaysOn Dictionary Keys)</li>
                    <li> VPN (solution uses a VPN plugin or NetworkExtension,</li>
                    so the VPNSubType key is required (see below)).
                </ul>
            </td>
        </tr>
        <tr>
            <td colspan="2"><strong><center>Vendor Configurations</strong>
                <center>
            </td>
        </tr>
        <tr>
            <td><strong>Key</strong></td>
            <td></td>
        </tr>
        <tr>
            <td><strong>Value</strong></td>
            <td></td>
        </tr>
    </tbody>
</table>


## <strong> Certificate Install </strong>

This configurations can be used to install certificate on an iOS device.

<i>Please note that * sign represents required fields of data.</i>

<table style="width: 100%;">
    <colgroup>
        <col>
            <col>
    </colgroup>
    <tbody>
        <tr>
            <th><strong>Data keys of Policy</strong></th>
            <th>Description</th>
        </tr>
        <tr>
        </tr>
        <tr>
            <td><strong>Certificate name</strong></td>
            <td>The file name of the enclosed certificate.
            </td>
        </tr>
        <tr>
            <td><strong>Certificate file</strong></td>
            <td>The base64 representation of the payload with a line length of 52.</td>
        </tr>
        <tr>
            <td><strong>Certificate Password </strong></td>
            <td> For PKCS#12 certificates, contains the password to the identity.</td>
        </tr>
        <tr>
            <td colspan="2">
                <center><strong>Certificate type</strong>
                    <br>The PayloadType of a certificate payload must be one of the following:</center>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <table style="width: 100%;">
                    <tbody>
                        <tr>
                            <th>Payload type</th>
                            <th>Container format</th>
                            <th>Certificate type</th>
                        </tr>
                        <tr>
                            <td>com.apple.security.root</td>
                            <td>PKCS#1(.cer)</td>
                            <td>Alias for com.apple.security.pkcs1.</td>
                        </tr>
                        <tr>
                            <td>com.apple.security.pkcs1 </td>
                            <td>PKCS#1(.cer)</td>
                            <td>DER-encoded certificate without private key. May contain root certificates.</td>
                        </tr>
                        <tr>
                            <td>com.apple.security.pem</td>
                            <td>PKCS#1(.cer)</td>
                            <td>PEM-encoded certificate without private key. May contain root certificates</td>
                        </tr>
                        <tr>
                            <td>com.apple.security.pkcs12</td>
                            <td>PKCS#12(.p12)</td>
                            <td>Password-protected identity certificate. Only one certificate may be included.</td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table>

## <strong> Font Install </strong>

This configurations can be used to add an additional font to an iOS device.

<i>Please note that * sign represents required fields of data.</i>

<table style="width: 100%;">
    <colgroup>
        <col>
            <col>
    </colgroup>
    <tbody>
        <tr>
            <th><strong>Data keys of Policy</strong></th>
            <th>Description</th>
        </tr>
        <tr>
            <td><strong>Font name</strong></td>
            <td>The user-visible name for the font. This field is replaced by the actual name of the font after installation.
            </td>
        </tr>
        <tr>
            <td><strong>Font file</strong></td>
            <td>The contents of the font file.<br>
            <i>Each payload must contain exactly one font file in TrueType (.ttf) or OpenType (
            .otf) format. Collection formats (.ttc or
            .otc) are not supported.</i>
            </td>
        </tr>
    </tbody>
</table>




## <strong> App Lock </strong>

Enforce iOS device to lock to a single application.

<i> This configuration will be applied only on Supervised devices.</i>


<table style="width: 100%;">
    <colgroup>
        <col>
            <col>
    </colgroup>
    <tbody>
        <tr>
            <th><strong>Data keys of Policy</strong></th>
            <th>Description</th>
        </tr>
        <tr>
            <td><strong>Application Identifier</strong></td>
            <td>The bundle identifier of the application.
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <center><strong>Application Identifier</strong></center>
            </td>
        </tr>
        <tr>
            <td><strong>Disable touch</strong></td>
            <td> If true, the touch screen is disabled.
                <br> Also, available in tvOS 10.2 and later to lock the touch pad on the remote.
            </td>
        </tr>
        <tr>
            <td><strong>Disable device rotation</strong></td>
            <td>If true, device rotation sensing is disabled.
            </td>
        </tr>
        <tr>
            <td><strong>Disable volume buttons</strong></td>
            <td>If true, the volume buttons are disabled.</td>
        </tr>
        <tr>
            <td>
                <center><strong>Disable ringer switch</strong></center>
            </td>
            <td> If true, the ringer switch is disabled. When disabled, the ringer behavior depends on what position the switch was in when it was first disabled.</td>
        </tr>
        <tr>
            <td><strong>Disable sleep wake button</strong></td>
            <td>If true, the sleep/wake button is disabled.
            </td>
        </tr>
        <tr>
            <td><strong>Disable auto lock</strong></td>
            <td>If true, the device will not automatically go to sleep after an idle period.
                <br> Also, available in tvOS 10.2 and later.</td>
        </tr>
        <tr>
            <td><strong>Enable voice over</strong></td>
            <td>If true, VoiceOver is turned on.
                <br> Also, available in tvOS 10.2 and later
            </td>
        </tr>
        <tr>
            <td><strong>Enable zoom</strong></td>
            <td>If true, Zoom is turned on.
                <br> Also, available in tvOS 10.2 and later</td>
        </tr>
        <tr>
            <td><strong>Enable invert colors</strong></td>
            <td>If true, Invert Colors is turned on.
                <br> Also, available in tvOS 10.2 and later.</td>
        </tr>
        <tr>
            <td><strong>Enable assistive touch</strong></td>
            <td> If true, AssistiveTouch is turned on</td>
        </tr>
        <tr>
            <td>
                <center><strong>Enable speak selection</strong></center>
            </td>
            <td>If true, Speak Selection is turned on.</td>
        </tr>
        <tr>
            <td><strong>Enable mono audio</strong></td>
            <td>If true, the sleep/wake button is disabled.
            </td>
        </tr>
        <tr>
            <td><strong>Enable mono audio</strong></td>
            <td>If true, Mono Audio is turned on</td>
        </tr>
        <tr>
            <td colspan="2">
                <center><strong>Application User Enabled 
                    Options</strong></center>
            </td>
        </tr>
        <tr>
            <td><strong>Allow voice over adjustment</strong></td>
            <td>If true, allow VoiceOver adjustment.
                <br> Also, available in tvOS 10.2 and later.</td>
        </tr>
        <tr>
            <td><strong>Allow zoom adjustment</strong></td>
            <td>If true, allow Zoom adjustment.
                <br> Also, available in tvOS 10.2 and later.</td>
        </tr>
        <tr>
            <td><strong>Allow invert colors adjustment</strong></td>
            <td>If true, allow Invert Colors adjustment.
                <br> Also, available in tvOS 10.2 and later.</td>
        </tr>
        <tr>
            <td><strong>Allow assistive touch adjustment</strong></td>
            <td>If true, allow AssistiveTouch adjustment.</td>
        </tr>
    </tbody>
</table>