---
bookCollapseSection: true
weight: 1
---

# Android device policies

## Add a policy


1.Go to devicemgt portal and click on Add policies (https://{IP}:{port}/devicemgt/policy/add) 

![image](11.png)

2.Click on Android from "DEVICE TYPES"

![image](1.png)

3.Create your policy. In this tutorial, let's create a passcode policy.
   After defining the settings, click CONTINUE.
   
      A profile in the context of Entgra IoT Server refers to a collection of policies.
      For example, in this use case you are only creating one policy that is the passcode policy.
      If you want to, you can add an restrictions policy too. 
      All these policies will be bundled as a profile and then pushed to the devices.
      
4.Select the policy type.

There are two types of policies.
<ul style="list-style-type:upper-roman;">
    <li><strong>General Policy:</strong> General policy is applied to the device by default.</li>
    <li><strong>Corrective Policy:</strong> Corrective policy is applied to the device when the 
    general policy is violated. When the general policy is not violated the correctiv policy is 
    disabled.
    </li>   
</ul>

If you wish to apply a corrective policy with a general policy, 

First apply a corrective policy by selecting the policy type as the corrective policy.

![image](111.png)

Then apply a general policy by selecting the policy type as the general policy.

![image](112.png)

Select the corrective policy to be applied when this general policy is violated.

![image](112.png)

5.Click CONTINUE.
   
6.Define the user groups that the passcode policy needs to be assigned to:
      
      Select the set user role/s or set user/s option and then select the users/roles from the item 
      list.
      Let's select set user role/s and then select ANY. 
      
![image](13.png)

      Select the Select Groups option and then select the groups from the item 
      list.
![image](2.png)

7.Click CONTINUE.

8.Define the policy name and the description of the policy.

![image](14.png)

9.Click SAVE AND PUBLISH to save and publish the configured profile as an active policy to the 
database.
           
    If you SAVE the configured profile, it will be in the inactive state and will not be applied 
    to any devices. 
    If you SAVE AND PUBLISH the configured profile of policies, it will be in the active state.   
10.To publish the policy to the existing devices, click APPLY CHANGES TO DEVICES from the policy 
  management page.
  
  
## View a policy

1.Go to devicemgt portal and click on View policies 
(https://{IP}:{port}/devicemgt/devicemgt/policies
![image](17.png)

## Publish a policy

1.Click View under POLICIES to get the list of the available policies.

![image](17.png)

2.Click Select to select the policy or policies that are not in the publish state and you wish to 
publish.

![image](22.png)

3.Click Publish.

![image](21.png)

## Unpublish a policy

1.Go to devicemgt portal and click on View policies 
(https://{IP}:{port}/devicemgt/devicemgt/policies

![image](17.png)

2.Click Select to select the policy or policies that are not in the publish state and you wish to 
publish.

![image](19.png)

3.Click Unpublish

![image](23.png)

4.Click YES to confirm that you want to unpublish the policy.

![image](24.png)

5.Now your policy is unpublished and is in the inactive/updated state. Therefore, the policy will
 not be applied on devices that enroll newly with Entgra IoT Server.
 
![image](25.png)

## Verify the policy enforced on a device

1.Click View under DEVICES

![image](26.png)

2.Click on your device to view the device details. Click Policy Compliance.

3.You will see the policy that is currently applied to your device.

## Manage the policy priority order

You can change the priority order of the policies and make sure the policy that you want is applied 
on devices that register with Entgra IoT Server. 

1.Click View under POLICIES to get the list of the available policies.

![image](17.png)

2.Click POLICY PRIORITY.

![image](27.png)

3.Manage the policy priority:
    Drag and drop the policies to prioritize the policies accordingly.
    Manage the policy priority order by defining the order using the edit box.   
    ![image](28.png)
    
4.Click SAVE NEW PRIORITY ORDER to save the changes. 

5.Click APPLY CHANGES to push the changes, to the existing devices.

## Updating a Policy

1.Click View under POLICIES to get the list of the available policies.

![image](17.png)

2.On the policy, you wish to edit, click on the edit icon.

![image](29.png)

3.Edit the policy:

    a.Edit current profile and click CONTINUE.
    b.Edit assignment groups and click CONTINUE.   
    c.Optionally, edit the policy name and description.
    
  Click SAVE to save the configured profile or click SAVE AND PUBLISH to save and publish the 
  configured profile as an active policy to the database.

## Description of Available Android Policies  
  
<table style="width: 100%;">
    <colgroup>
        <col>
            <col>
    </colgroup>
    <tbody>
        <tr>
            <th>Policy</th>
            <th>Description</th>
        </tr>
        <tr>
            <td><strong><a href ="https://entgra-documentation.gitlab.io/v3.7.0/docs/guide-to-work-with-the-product/device-management-guide/android-device/android-device-policies/policy-description/#strong-passcode-policy-strong">Passcode 
              Policy</a></strong></td>
            <td>Enforce a configured passcode policy on Android devices. Once this profile is applied, the device owners won't be able to modify the password settings on their devices.
            </td>
        </tr>
        <tr>
            <td><strong><a href = "">Restrictions</a></strong></td>
            <td>Restrict predefined settings on Android devices. Once this profile is applied, the device owners won't be able to modify the configured settings on their devices.</td>
        </tr>
        <tr>
            <td><strong><a href = "https://entgra-documentation.gitlab.io/v3.7.0/docs/guide-to-work-with-the-product/device-management-guide/android-device/android-device-policies/policy-description/#strong-encryption-settings-strong">Encryption Settings</a></strong></td>
            <td>Encrypt data on an Android device when the device is locked and make it readable when the device is unlocked. Once this profile is applied, the device owners won't be able to modify the configured settings on their devices.</td>
        </tr>
        <tr>
            <td><strong><a href = "https://entgra-documentation.gitlab.io/v3.7.0/docs/guide-to-work-with-the-product/device-management-guide/android-device/android-device-policies/policy-description/#strong-wi-fi-settings-strong">Wi-Fi Settings</a></strong></td>
            <td>Configure the Wi-Fi settings on Android devices. Once this profile is applied, the device owners won't be able to modify the configured settings on their devices.</td>
        </tr>
        <tr>
            <td><strong><a href = "https://entgra-documentation.gitlab.io/v3.7.0/docs/guide-to-work-with-the-product/device-management-guide/android-device/android-device-policies/policy-description/#strong-global-proxy-settings-strong">Global Proxy Settings</a></strong></td>
            <td>This configurations can be used to set a network-independent global HTTP proxy on an Android device. Once this configuration profile is installed on a device, all the network traffic will be routed through the proxy server.</td>
        </tr>
        <tr>
            <td><strong><a href = "https://entgra-documentation.gitlab.io/v3.7.0/docs/guide-to-work-with-the-product/device-management-guide/android-device/android
            -device-policies/policy-description/#strong-virtual-private-network-strong">Virtual 
            Private Network</a></strong></td>
            <td>These configurations can be used to define settings for connecting to your POP or IMAP email accounts. Once this configuration profile is installed on an iOS device, corresponding users will not be able to modify these settings on their devices.</td>
            <tr>
                <td><strong><a href = "">Certificate Install Settings</a></strong></td>
                <td>Restrict predefined settings on Android devices. Once this profile is applied, the device owners won't be able to modify the configured settings on their devices.</td>
            </tr>
            <tr>
                <td><strong><a href = "https://entgra-documentation.gitlab.io/v3.7.0/docs/guide-to-work-with-the-product/device-management-guide/android-device/android-device-policies/policy-description/#strong-work-profile-configurations-strong">Work-Profile Configurations</a></strong></td>
                <td>Configure these settings to manage the applications in the work profile.</td>
            </tr>
            <tr>
                <td><strong><a href = "https://entgra-documentation.gitlab.io/v3.7.0/docs/guide-to-work-with-the-product/device-management-guide/android-device/android-device-policies/policy-description/#strong-cosu-profile-configuration-strong">COSU Profile Configuration</a></strong></td>
                <td>This policy can be used to configure the profile of COSU Devices.</td>
            </tr>
            <tr>
                <td><strong><a href = "https://entgra-documentation.gitlab.io/v3.7.0/docs/guide-to-work-with-the-product/device-management-guide/android-device/android-device-policies/policy-description/#strong-application-restriction-settings-strong">Application Restriction Settings</a></strong></td>
                <td>Blacklist or whitelist mobile application for Android devices.</td>
            </tr>
            <tr>
                <td><strong><a href = "https://entgra-documentation.gitlab.io/v3.7.0/docs/guide-to-work-with-the-product/device-management-guide/android-device/android-device-policies/policy-description/#strong-runtime-permission-policy-cosu--work-profile-strong">Runtime Permission Policy (COSU / Work Profile)</a></strong></td>
                <td>This configuration can be used to set a runtime permission policy to an Android Device.</td>
            </tr>
            <tr>
                <td><strong><a href = "https://entgra-documentation.gitlab.io/v3.7.0/docs/guide-to-work-with-the-product/device-management-guide/android-device/android-device-policies/policy-description/#strong-system-update-policy-cosu-strong">System Update Policy (COSU)</a></strong></td>
                <td>Configure the settings to install system updates on single-purpose or COSU devices.</td>
            </tr>
            <tr>
                <td><strong><a href = "https://entgra-documentation.gitlab.io/v3.7.0/docs/guide-to-work-with-the-product/device-management-guide/android-device/android-device-policies/policy-description/#strong-enrollment-application-install-strong">Enrollment Application Install</a></strong></td>
                <td>Enforce applications to be installed during Android device enrollment.</td>
            </tr>
    </tbody>
</table>