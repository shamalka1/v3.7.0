---
bookCollapseSection: true
weight: 1
---
# Creating a new version of the Mobile Application


When new features need to be added or features need to be removed from an application a new version of the application needs to be created. In this tutorial let's look at how to create a new version of an Android application. The same steps apply when creating a new version of an iOS application.

Follow the steps given below.

1.  Navigate to the App Publisher using the following URL:** `https://<IOTS_HOST>:9443/publisher`**
2.  [Enter the username and password, and sign in](https://entgra-documentation.gitlab.io/v3.7.0/docs/using-entgra-iot-server/installation-guide/Running-the-Product/#accessing-the-entgra-iot-server-management-console).


3.  Click the mobile application you wish to update.

4.  Click **Create New Version**.  
    Example:   
    ![image](352813442.png)
5.  Select the new APK file, and click **Continue**.

    

    

    Do not rename the previous APK file and try uploading it to create a new version. This will not work as the server reads the metadata in the generated APK file to create a new application. Therefore, you need to create a new version of the APK file.

    

    

    Example: Select the new APK file for the Catalog application that is for version 1.2.0.  
    ![image](352813448.png)

6.  Update the application details.  

    *   **Display Name **- The name of the application that is displayed to the user.
    *   **Description** -  A summarized description of the application.
    *   **Recent Changes** - Optional. A summarized description of what's new in the application when compared to the previous version.
    *   **Banner**  - Upload the application banner.
    *   **Screenshots**  - Upload the screenshots of the application so that the user can get an understanding of what the app offers. A maximum of four screenshots are allowed.
    *   **Icon file**  - The image that will be used as the application icon in the Store and when the application is installed on a device.
7.  Click **Create New Version**. The created app will appear in the created list of apps.

    

    

    After creating the mobile app, authorized users can review, approve, and publish the mobile application. For more information, see [Mobile Application Lifecycle Management](https://entgra-documentation.gitlab.io/v3.7.0/docs/using-entgra-iot-server/Managing-Mobile-Applications/#mobile-application-lifecycle-management).

