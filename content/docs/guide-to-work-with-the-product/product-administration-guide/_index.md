---
bookCollapseSection: true
weight: 6
---

# Product Administration

Entgra Internet of Things Server (Entgra IoT Server) is shipped with default configurations that will allow you to download, install and get started with your product instantly. However, when you go into production, it is recommended to change some of the default settings to ensure that you have a robust system that is suitable for your operational needs. Also, you may have specific use cases that require specific configurations to the server. [Entgra Enterprise Mobility (Entgra EMM) capabilities are bundled with Entgra IoT Server](https://entgra.io/emm#) too.


